#ifndef KM_STRING_BUILDER_H_INCLUDED
#define KM_STRING_BUILDER_H_INCLUDED

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "kmstringview.h"

typedef struct string_builder {
  size_t size;
  size_t capacity;
  char *data;
} string_builder;

string_builder *sb_new();

/**
 * Appends the given char to the sequence
*/
void sb_append_c(string_builder *sb, const char c);

/**
 * Appends the given char array to the sequence
*/
void sb_append_ch(string_builder *sb, const char *c);

/**
 * Appends the given substring to the sequence
*/
void sb_append_sv(string_builder *sb, string_view sv);

/**
 * Appends a newline to the sequence
*/
void sb_append_newline(string_builder *sb);

/**
 * Appends the given int value to the sequence
*/
void sb_append_i(string_builder *sb, int value);

/**
 * Appends the given float value to the sequence
*/
void sb_append_f(string_builder *sb, float value);

/**
 * Appends the given double value to the sequence
*/
void sb_append_d(string_builder *sb, double value);

/**
 * Reverses the sequence
*/
void sb_reverse(string_builder *sb);

/**
 * Copy the contents to the destination
*/
void sb_to_str(string_builder *sb, char *dest);

string_builder *sb_new() {
  string_builder* builder = (string_builder *)malloc(sizeof(string_builder));
  builder->capacity = 128;
  builder->size = 0;
  builder->data = (char *)malloc(sizeof(char) * builder->capacity);
  return builder;
}

static void sb_resize(string_builder *sb, size_t capacity) {
  char *data = (char *)realloc(sb->data, sizeof(char) * capacity);
  
  if (data) {
    sb->data = data;
    sb->capacity = capacity;
  }
}

void sb_free(string_builder *sb) {
  free(sb->data);
  free(sb);
}

void sb_append_c(string_builder *sb, const char c) {
  if (sb->size == sb->capacity) {
    sb_resize(sb, sb->capacity * 2);
  }

  sb->data[sb->size] = c;
  ++sb->size;
}

void sb_append_ch(string_builder *sb, const char *c) {
  int len = strlen(c);

  for (int i = 0; i < len; ++i) {
    if (c[i] != '\0') sb_append_c(sb, c[i]);
  }
}

void sb_append_sv(string_builder *sb, string_view sv) {
  int len = sv.size;

  for (int i = 0; i < len; ++i) {
    sb_append_c(sb, sv.data[i]);
  }
}

void sb_append_newline(string_builder *sb) {
  sb_append_c(sb, '\n');
}

void sb_append_i(string_builder *sb, int value) {
  char temp[32];
  snprintf(temp, 32, "%d", value);
  char *temp1 = (char *)malloc(strlen(temp) + 1);
  memcpy(temp1, temp, strlen(temp) + 1);
  sb_append_ch(sb, temp1);
  free(temp1);
}

void sb_append_ui(string_builder *sb, unsigned int value) {
  char temp[32];
  snprintf(temp, 32, "%u", value);
  char *temp1 = (char *)malloc(strlen(temp) + 1);
  memcpy(temp1, temp, strlen(temp) + 1);
  sb_append_ch(sb, temp1);
  free(temp1);
}

void sb_append_ll(string_builder *sb, long long value) {
  char temp[64];
  snprintf(temp, 64, "%lld", value);
  char *temp1 = (char *)malloc(strlen(temp) + 1);
  memcpy(temp1, temp, strlen(temp) + 1);
  sb_append_ch(sb, temp1);
  free(temp1);
}

void sb_append_ull(string_builder *sb, unsigned long long value) {
  char temp[64];
  snprintf(temp, 64, "%lld", value);
  char *temp1 = (char *)malloc(strlen(temp) + 1);
  memcpy(temp1, temp, strlen(temp) + 1);
  sb_append_ch(sb, temp1);
  free(temp1);
}

void sb_append_f(string_builder *sb, float value) {
  int len = snprintf(NULL, 0, "%f", value);
  char *result = (char *)malloc(len + 1);
  snprintf(result, len + 1, "%f", value);
  sb_append_ch(sb, result);
  free(result);
}

void sb_append_d(string_builder *sb, double value) {
  int len = snprintf(NULL, 0, "%f", value);
  char *result = (char *)malloc(len + 1);
  snprintf(result, len + 1, "%f", value);
  sb_append_ch(sb, result);
  free(result);
}

void sb_reverse(string_builder *sb) {
  int len = sb->size;

  if (len == 0) return;

  char *temp = (char *)malloc(sb->capacity);
  strcpy(temp, sb->data);

  for (int i = 0; i < len; ++i) {
    sb->data[i] = temp[len - 1 - i];
  }

  free(temp);
}

void sb_to_str(string_builder *sb, char *dest) {
  memcpy(dest, sb->data, sb->size);
  dest[sb->size] = '\0';
}

char *sb_copy_data(string_builder *sb) {
    char *dst = (char *)malloc(sb->size + 1);

    memcpy(dst, sb->data, sb->size);
    dst[sb->size] = '\0';
    return dst;
}

#endif // KM_STRING_BUILDER_H_INCLUDED