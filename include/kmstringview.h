#ifndef KM_STRING_VIEW_H_INCLUDED
#define KM_STRING_VIEW_H_INCLUDED

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

typedef struct string_view {
  const char *data;
  size_t size;
} string_view;

string_view sv_new(const char *str);

/**
 * Gets the char at the specified index. If index is out of bounds a null char will be returned
*/
char sv_at(string_view sv, size_t index);

/**
 * Gets the first character. If the view is empty a null char will be returned
*/
char sv_front(string_view sv);

/**
 * Gets the last character. If the view is empty a null char will be returned
*/
char sv_back(string_view sv);

/**
 * Checks whether the view is empty
*/
bool sv_empty(string_view sv);

/**
 * Checks if the view starts with the given prefix
*/
bool sv_starts_with(string_view sv, string_view prefix);

/**
 * Checks if the view ends with the given prefix
*/
bool sv_ends_with(string_view sv, string_view prefix);

/**
 * Removes whitespace from both ends of the view and returns the result
*/
string_view sv_trim(string_view sv);

/**
 * Removes whitespace from the beginning of the view and returns the result
*/
string_view sv_trim_left(string_view sv);

/**
 * Removes whitespace from the end of the view and returns the result
*/
string_view sv_trim_right(string_view sv);

/**
 * Shrinks the view by moving its start forward and returns what was chopped
*/
string_view sv_chop_left(string_view *sv, size_t n);

/**
 * Shrinks the view by moving its end backward and returns what was chopped
*/
string_view sv_chop_right(string_view *sv, size_t n);

/**
 * Shrinks the view by moving its start forward
*/
void sv_remove_prefix(string_view *sv, size_t n);

/**
 * Shrinks the view by moving its end backward
*/
void sv_remove_suffix(string_view *sv, size_t n);

/**
 * Swaps the contents of the views
*/
void sv_swap(string_view *sv0, string_view *sv1);

/**
 * Returns the first index at which the given char can be found or -1 if it is not present 
*/
int sv_index_of(string_view sv, char c);

/**
 * Returns the last index at which the given char can be found or -1 if it is not present
*/
int sv_last_index_of(string_view sv, char c);

/**
 * Returns the last index at which the given substring can be found or -1 if it is not present
*/
int sv_last_index_of_sv(string_view sv, string_view c);

/**
 * Returns a view of the substring start...start + count
*/
string_view sv_substr(string_view sv, size_t start, size_t count);

/**
 * Checks if the view contains the given character
*/
bool sv_contains(string_view sv, char c);

/**
 * Checks if the view contains the substring
*/
bool sv_contains_sv(string_view sv, string_view c);

/**
 * Compares two views
*/
bool sv_compare(string_view sv0, string_view sv1);

/**
 * Returns the first index at which the given char can be found starting at the given position or -1 if it is not present 
*/
int sv_find(string_view sv, char c, size_t pos);

/**
 * Returns the first index at which the given substring can be found starting at the given position or -1 if it is not present 
*/
int sv_find_sv(string_view sv, string_view c, size_t pos);

/**
 * Copies the content of the view to a char array
*/
void sv_copy(char *dest, string_view sv);

/**
 * Copies the content of the view to a char array
*/
void sv_copy_s(char *dest, string_view sv, size_t size);

#define sv(string) sv_new(string)

static string_view sv_construct(const char *data, size_t size) {
  string_view sv;
  sv.data = data;
  sv.size = size;
  return sv;
} 

string_view sv_new(const char *str) {
  return sv_construct(str, strlen(str));
}

char sv_at(string_view sv, size_t index) {
  if (index >= sv.size) {
    return '\0';
  }

  return sv.data[index];
}

char sv_front(string_view sv) {
  if (sv.size == 0) {
    return '\0';
  }

  return sv.data[0];
}

char sv_back(string_view sv) {
  if (sv.size == 0) {
    return '\0';
  }

  return sv.data[sv.size - 1];
}

bool sv_empty(string_view sv) {
  return sv.size == 0;
}

bool sv_starts_with(string_view sv, string_view prefix) {
  if (prefix.size > sv.size) return false;
  return sv_compare(sv_construct(sv.data, prefix.size), prefix);
}

bool sv_ends_with(string_view sv, string_view suffix) {
  if (suffix.size > sv.size) return false;
  return sv_compare(sv_construct(sv.data + sv.size - suffix.size, suffix.size), suffix);
}

string_view sv_trim_left(string_view sv) {
  size_t i = 0;
  while (i < sv.size && (sv.data[i] == ' ' || sv.data[i] == '\f' || sv.data[i] == '\n' || sv.data[i] == '\r' || sv.data[i] == '\v')) {
    i += 1;
  }
  return sv_construct(sv.data + i, sv.size - i);
}

string_view sv_trim_right(string_view sv) {
  size_t i = 0;
  while (i < sv.size && (sv.data[sv.size - 1 - i] == ' ' || sv.data[sv.size - 1 - i] == '\f' || sv.data[sv.size - 1 - i] == '\n' || sv.data[sv.size - 1 - i] == '\r' || sv.data[sv.size - 1 - i] == '\v')) {
    i += 1;
  }
  return sv_construct(sv.data, sv.size - i);
}

string_view sv_chop_left(string_view *sv, size_t n) {
  if (n > sv->size) {
    sv->data += sv->size;
    sv->size = 0;
    return sv_construct(sv->data, 0);
  }

  string_view res = sv_construct(sv->data, n);
  sv->data += n;
  sv->size -= n;
  return res;
}

string_view sv_chop_right(string_view *sv, size_t n) {
  if (n > sv->size) {
    sv->size = 0;
    return sv_construct(sv->data, 0);
  }

  string_view res = sv_construct(sv->data + sv->size - n, n);
  sv->size -= n;
  return res;
}

void sv_remove_prefix(string_view *sv, size_t n) {
 if (n > sv->size) {
    sv->data += sv->size;
    sv->size = 0;
  } else {
    sv->data += n;
    sv->size -= n;
  }
}

void sv_remove_suffix(string_view *sv, size_t n) {
 if (n > sv->size) {
    sv->size = 0;
  } else {
    sv->size -= n;
  }
}

void sv_swap(string_view *sv0, string_view *sv1) {
  const char *sv0data = sv0->data;
  const char *sv1data = sv1->data;
  size_t sv0size = sv0->size;
  size_t sv1size = sv1->size;

  sv0->data = sv1data;
  sv0->size = sv1size;
  sv1->data = sv0data;
  sv1->size = sv0size;
}

string_view sv_trim(string_view sv) {
  return sv_trim_left(sv_trim_right(sv));
}

bool sv_compare(string_view sv0, string_view sv1) {
  if (sv0.size != sv1.size) return false;
  else {
    return memcmp(sv0.data, sv1.data, sv0.size) == 0;
  }
}

int sv_find(string_view sv, char c, size_t pos) {
  size_t i = pos;

  while (i < sv.size && sv.data[i] != c) {
    i += 1;
  }
  
  return i < sv.size ? (int) i : -1;
}

int sv_find_sv(string_view sv, string_view c, size_t pos) {
  if (c.size > sv.size) {
    return -1;
  }

  int containsIdx = -1;
  size_t i = pos;
  size_t j = 0;
  while (i < sv.size) {
    bool idxF = true;
    
    while (j < c.size) {
      if (j + i >= sv.size) {
        idxF = false;
        break;
      }

      if (sv.data[i + j] != c.data[j]) {
        idxF = false;
        break;
      }
      ++j;
    }

    if (idxF) {
      containsIdx = i;
      break;
    }

    ++i;
  }

  return containsIdx;
}

int sv_index_of(string_view sv, char c) {
  size_t i = 0;

  while (i < sv.size && sv.data[i] != c) {
    i += 1;
  }
  
  return i < sv.size ? (int) i : -1;
}

int sv_index_of_sv(string_view sv, string_view c) {
  if (c.size > sv.size) {
    return -1;
  }

  int containsIdx = -1;
  size_t i = 0;
  size_t j = 0;
  while (i < sv.size) {
    bool idxF = true;
    
    while (j < c.size) {
      if (j + i >= sv.size) {
        idxF = false;
        break;
      }

      if (sv.data[i + j] != c.data[j]) {
        idxF = false;
        break;
      }
      ++j;
    }

    if (idxF) {
      containsIdx = i;
      break;
    }

    ++i;
  }

  return containsIdx;
}

int sv_last_index_of(string_view sv, char c) {
  size_t i = 0;

  while (i < sv.size && sv.data[sv.size - i - 1] != c) {
    i += 1;
  }
  
  return i < sv.size ? (int) (sv.size - 1 - i) : -1;
}

int sv_last_index_of_sv(string_view sv, string_view c) {
  if (c.size > sv.size) {
    return -1;
  }

  int containsIdx = -1;
  size_t i = sv.size - 1;
  size_t j = 0;
  while (i > 0) {
    bool idxF = true;
    
    while (j < c.size) {
      if (j + i >= sv.size) {
        idxF = false;
        break;
      }

      if (sv.data[i + j] != c.data[j]) {
        idxF = false;
        break;
      }
      ++j;
    }

    if (idxF) {
      containsIdx = i;
      break;
    }

    --i;
  }

  return containsIdx;
}

string_view sv_substr(string_view sv, size_t start, size_t count) {
  string_view res = sv_construct(sv.data, sv.size);
  res.data += start;
  res.size = count > sv.size ? sv.size : count;
  return res;
}

bool sv_contains(string_view sv, char c) {
  return sv_index_of(sv, c) >= 0;
}

bool sv_contains_sv(string_view sv, string_view c) {
  return sv_index_of_sv(sv, c) >= 0;
}

void sv_copy(char *dest, string_view sv) {
  memcpy(dest, sv.data, sv.size);
  dest[sv.size] = '\0';
  return;
}

void sv_copy_s(char *dest, string_view sv, size_t size) {
  memcpy(dest, sv.data, size > sv.size ? sv.size : size);
  dest[sv.size] = '\0';
  return;
}

#endif // KM_STRING_VIEW_H_INCLUDED