/*
  TODO:
    - queue
    - set
    - map
*/

#ifndef KM_HELPER_H_
#define KM_HELPER_H_

#include <stdint.h>
#include <stdbool.h>

#define KM_SAFE_ITER_INDEX_REACH 65536

/**
 * It does the same as printf but adds a new line to the end of the message
*/
#define printfln(format, ...) printf(format "\n", __VA_ARGS__)

/**
 * Prints message to the stdout with a new line at the end
*/
#define println(format) printf(format "\n")

/**
 * Calculates the length of the given cstring. It does not add to the count the null terminator (\0) and it also handles utf8.
 */
int strlength(const char *str) {
  // Checks if it's a null pointer
  if (str == NULL) {
    return 0;
  }

  int len = 0; // Amount of actual characters
  
  int i = 0;
  while (i < KM_SAFE_ITER_INDEX_REACH) {
    size_t bt = str[i];

    if (bt == '\0') { // If byte is null terminator that's the end of the cstring
      break;
    }

    if ((bt & 0x80) == 0) { // If the leftmost bit is 0 then it's a 1 byte character
      ++len;
    } else if ((bt & 0xE0) == 0xC0) { // If the 3 leftmost bits are 110 then it's a 2 byte character
      ++len;
      ++i;
    } else if ((bt & 0xF0) == 0xE0) { // If the 4 leftmost bits are 1110 then it's a 3 byte character
      ++len;
      i += 2;
    } else if ((bt & 0xF8) == 0xF0) { // If the 5 leftmost bits are 11110 then it's a 4 byte character
      ++len;
      i += 3;
    } else if ((bt & 0xFC) == 0xF8) {
      ++len;
      i += 4;
    } else if ((bt & 0xFE) == 0xFC) {
      ++len;
      i += 5;
    }

    ++i;
  }

  return len;
}

/**
 * Calculates the length of the given wide cstring. It does not add to the count the null terminator (\0) and it also handles utf8.
 */
int wstrlength(const wchar_t *wstr) {
  // Checks if it's a null pointer
  if (wstr == NULL) {
    return 0;
  }

  int len = 0; // Amount of actual characters

  int i = 0;
  while (i < KM_SAFE_ITER_INDEX_REACH) {
    size_t bt = wstr[i];

    if (bt == '\0') { // If byte is null terminator that's the end of the cstring
      break;
    }

    if (wstr[i + 1] != '\0') {
      size_t b = bt << 8 | wstr[i + 1];

      if ((b & 0xFC00) < 0xD800 || (b & 0xFC00) > 0xDFFF) {
        ++i;
      }
      ++len;
    } else {
      ++len;
    }
    
    ++i;
  }

  return len;
}

/**
 * Calculates the content length in the given cstring by ignoring whitespace. It does not add to the count the null terminator (\0) and it also handles utf8.
 */
int strcntlength(const char *str) {
  // Checks if it's a null pointer
  if (str == NULL) {
    return 0;
  }

  int len = 0; // Amount of actual characters
  
  int i = 0;
  while (i < KM_SAFE_ITER_INDEX_REACH) {
    int bt = str[i];

    if (bt == '\0') { // If byte is null terminator that's the end of the cstring
      break;
    }

    if ((bt & 0x80) == 0) { // If the leftmost bit is 0 then it's a 1 byte character
      if (bt != ' ' && bt != '\f' && bt != '\r' && bt != '\n' && bt != '\t') { // Prevent whitespace characters from adding to the length
        ++len;
      }
    } else if ((bt & 0xE0) == 0xC0) { // If the 3 leftmost bits are 110 then it's a 2 byte character
      ++len;
      ++i;
    } else if ((bt & 0xF0) == 0xE0) { // If the 4 leftmost bits are 1110 then it's a 3 byte character
      ++len;
      i += 2;
    } else if ((bt & 0xF8) == 0xF0) { // If the 5 leftmost bits are 11110 then it's a 4 byte character
      ++len;
      i += 3;
    } else if ((bt & 0xFC) == 0xF8) {
      ++len;
      i += 4;
    } else if ((bt & 0xFE) == 0xFC) {
      ++len;
      i += 5;
    }

    ++i;
  }

  return len;
}

/**
 * Calculates the content length in the given wide cstring by ignoring whitespace. It does not add to the count the null terminator (\0) and it also handles utf8.
 */
int wstrcntlength(const wchar_t *wstr) {
  // Checks if it's a null pointer
  if (wstr == NULL) {
    return 0;
  }

  int len = 0; // Amount of actual characters

  int i = 0;
  while (i < KM_SAFE_ITER_INDEX_REACH) {
    size_t bt = wstr[i];

    if (bt == '\0') { // If byte is null terminator that's the end of the cstring
      break;
    }

    if (bt == ' ' || bt == '\r' || bt == '\t' || bt == '\f') {
      ++i;
    } else if (wstr[i + 1] != '\0') {
      size_t b = bt << 8 | wstr[i + 1];

      if ((b & 0xFC00) < 0xD800 || (b & 0xFC00) > 0xDFFF) {
        ++i;
      }
      ++len;
    } else {
      ++len;
    }
    
    ++i;
  }

  return len;
}

// Rust like Option

#define none() { .has_value = false }
#define some(vl) { .has_value = true, .value = vl }

#define option_new_type(type) typedef struct { bool has_value; type value; } option_ ## type; \

option_new_type(bool)

option_new_type(char)
option_new_type(int)
option_new_type(long)
option_new_type(float)
option_new_type(double)

option_new_type(int8_t)
option_new_type(int16_t)
option_new_type(int32_t)
option_new_type(int64_t)
option_new_type(uint8_t)
option_new_type(uint16_t)
option_new_type(uint32_t)
option_new_type(uint64_t)

// vector

typedef struct vec {
  void **_data;
  size_t _element_size;
  size_t _capacity;
  size_t _size;
} vec;

/**
 * Creates a new vector with elements of size element_size. Do not forget to free the vector (use vec_free)
*/
vec *vec_new(size_t _element_size) {
  vec *self = malloc(sizeof(vec));
  self->_capacity = 16;
  self->_size = 0;
  self->_element_size = _element_size;
  self->_data = malloc(_element_size * self->_capacity);
  return self;
}

static void vec_resize(vec *self, int _capacity) {
  void **_data = realloc(self->_data, self->_element_size * _capacity);

  if (_data) {
    self->_data = _data;
    self->_capacity = _capacity;
  }
}

/**
 * Add an item to the end of the vector
*/
void vec_add(vec *self, void *item) {
  if (self->_capacity == self->_size) {
    vec_resize(self, self->_capacity * 2);
  }
  self->_data[self->_size++] = item;
}

/**
 * Set item in vector at the given index. If the index is out of bounds nothing will happen
*/
void vec_set(vec *self, int index, void *item) {
  if (index >= 0 && index < self->_size) {
    self->_data[index] = item;
  }
}

/**
 * Get the item at the given index in the vector. If the index is out of bounds it will return NULL 
*/
void *vec_get(vec *self, int index) {
  if (index >= 0 && index < self->_size) {
    return self->_data[index];
  }

  return NULL;
}

/**
 * Get the _size of the vector
*/
int vec_size(vec *self) {
  return self->_size;
}

/**
 * Free the memory used by the vector
*/
void vec_free(vec *self) {
  free(self->_data);
  free(self);
}

// C++ String View implementation in C (with extra methods)

typedef struct string_view {
  const char *data;
  size_t size;
} string_view;

string_view sv_new(const char *str);

/**
 * Gets the char at the specified index. If index is out of bounds a null char will be returned
*/
char sv_at(string_view sv, size_t index);

/**
 * Gets the first character. If the view is empty a null char will be returned
*/
char sv_front(string_view sv);

/**
 * Gets the last character. If the view is empty a null char will be returned
*/
char sv_back(string_view sv);

/**
 * Checks whether the view is empty
*/
bool sv_empty(string_view sv);

/**
 * Checks if the view starts with the given prefix
*/
bool sv_starts_with(string_view sv, string_view prefix);

/**
 * Checks if the view ends with the given prefix
*/
bool sv_ends_with(string_view sv, string_view prefix);

/**
 * Removes whitespace from both ends of the view and returns the result
*/
string_view sv_trim(string_view sv);

/**
 * Removes whitespace from the beginning of the view and returns the result
*/
string_view sv_trim_left(string_view sv);

/**
 * Removes whitespace from the end of the view and returns the result
*/
string_view sv_trim_right(string_view sv);

/**
 * Shrinks the view by moving its start forward and returns what was chopped
*/
string_view sv_chop_left(string_view *sv, size_t n);

/**
 * Shrinks the view by moving its end backward and returns what was chopped
*/
string_view sv_chop_right(string_view *sv, size_t n);

/**
 * Shrinks the view by moving its start forward
*/
void sv_remove_prefix(string_view *sv, size_t n);

/**
 * Shrinks the view by moving its end backward
*/
void sv_remove_suffix(string_view *sv, size_t n);

/**
 * Swaps the contents of the views
*/
void sv_swap(string_view *sv0, string_view *sv1);

/**
 * Returns the first index at which the given char can be found or -1 if it is not present 
*/
int sv_index_of(string_view sv, char c);

/**
 * Returns the last index at which the given char can be found or -1 if it is not present
*/
int sv_last_index_of(string_view sv, char c);

/**
 * Returns the last index at which the given substring can be found or -1 if it is not present
*/
int sv_last_index_of_sv(string_view sv, string_view c);

/**
 * Returns a view of the substring start...start + count
*/
string_view sv_substr(string_view sv, size_t start, size_t count);

/**
 * Checks if the view contains the given character
*/
bool sv_contains(string_view sv, char c);

/**
 * Checks if the view contains the substring
*/
bool sv_contains_sv(string_view sv, string_view c);

/**
 * Compares two views
*/
bool sv_compare(string_view sv0, string_view sv1);

/**
 * Returns the first index at which the given char can be found starting at the given position or -1 if it is not present 
*/
int sv_find(string_view sv, char c, size_t pos);

/**
 * Returns the first index at which the given substring can be found starting at the given position or -1 if it is not present 
*/
int sv_find_sv(string_view sv, string_view c, size_t pos);

/**
 * Copies the content of the view to a char array
*/
void sv_copy(char *dest, string_view sv);

/**
 * Copies the content of the view to a char array
*/
void sv_copy_s(char *dest, string_view sv, size_t size);

#define sv(string) sv_new(string)

static string_view sv_construct(const char *data, size_t size) {
  string_view sv;
  sv.data = data;
  sv.size = size;
  return sv;
} 

string_view sv_new(const char *str) {
  return sv_construct(str, strlen(str));
}

char sv_at(string_view sv, size_t index) {
  if (index < 0 || index >= sv.size) {
    return '\0';
  }

  return sv.data[index];
}

char sv_front(string_view sv) {
  if (sv.size == 0) {
    return '\0';
  }

  return sv.data[0];
}

char sv_back(string_view sv) {
  if (sv.size == 0) {
    return '\0';
  }

  return sv.data[sv.size - 1];
}

bool sv_empty(string_view sv) {
  return sv.size == 0;
}

bool sv_starts_with(string_view sv, string_view prefix) {
  if (prefix.size > sv.size) return false;
  return sv_compare(sv_construct(sv.data, prefix.size), prefix);
}

bool sv_ends_with(string_view sv, string_view suffix) {
  if (suffix.size > sv.size) return false;
  return sv_compare(sv_construct(sv.data + sv.size - suffix.size, suffix.size), suffix);
}

string_view sv_trim_left(string_view sv) {
  int i = 0;
  while (i < sv.size && (sv.data[i] == ' ' || sv.data[i] == '\f' || sv.data[i] == '\n' || sv.data[i] == '\r' || sv.data[i] == '\v')) {
    i += 1;
  }
  return sv_construct(sv.data + i, sv.size - i);
}

string_view sv_trim_right(string_view sv) {
  int i = 0;
  while (i < sv.size && (sv.data[sv.size - 1 - i] == ' ' || sv.data[sv.size - 1 - i] == '\f' || sv.data[sv.size - 1 - i] == '\n' || sv.data[sv.size - 1 - i] == '\r' || sv.data[sv.size - 1 - i] == '\v')) {
    i += 1;
  }
  return sv_construct(sv.data, sv.size - i);
}

string_view sv_chop_left(string_view *sv, size_t n) {
  if (n > sv->size) {
    sv->data += sv->size;
    sv->size = 0;
    return sv_construct(sv->data, 0);
  }

  string_view res = sv_construct(sv->data, n);
  sv->data += n;
  sv->size -= n;
  return res;
}

string_view sv_chop_right(string_view *sv, size_t n) {
  if (n > sv->size) {
    sv->size = 0;
    return sv_construct(sv->data, 0);
  }

  string_view res = sv_construct(sv->data + sv->size - n, n);
  sv->size -= n;
  return res;
}

void sv_remove_prefix(string_view *sv, size_t n) {
 if (n > sv->size) {
    sv->data += sv->size;
    sv->size = 0;
  } else {
    sv->data += n;
    sv->size -= n;
  }
}

void sv_remove_suffix(string_view *sv, size_t n) {
 if (n > sv->size) {
    sv->size = 0;
  } else {
    sv->size -= n;
  }
}

void sv_swap(string_view *sv0, string_view *sv1) {
  const char *sv0data = sv0->data;
  const char *sv1data = sv1->data;
  size_t sv0size = sv0->size;
  size_t sv1size = sv1->size;

  sv0->data = sv1data;
  sv0->size = sv1size;
  sv1->data = sv0data;
  sv1->size = sv0size;
}

string_view sv_trim(string_view sv) {
  return sv_trim_left(sv_trim_right(sv));
}

bool sv_compare(string_view sv0, string_view sv1) {
  if (sv0.size != sv1.size) return false;
  else {
    return memcmp(sv0.data, sv1.data, sv0.size) == 0;
  }
}

int sv_find(string_view sv, char c, size_t pos) {
  int i = pos;

  while (i < sv.size && sv.data[i] != c) {
    i += 1;
  }
  
  return i < sv.size ? i : -1;
}

int sv_find_sv(string_view sv, string_view c, size_t pos) {
  if (c.size > sv.size) {
    return -1;
  }

  int containsIdx = -1;
  size_t i = pos;
  size_t j = 0;
  while (i < sv.size) {
    bool idxF = true;
    
    while (j < c.size) {
      if (j + i >= sv.size) {
        idxF = false;
        break;
      }

      if (sv.data[i + j] != c.data[j]) {
        idxF = false;
        break;
      }
      ++j;
    }

    if (idxF) {
      containsIdx = i;
      break;
    }

    ++i;
  }

  return containsIdx;
}

int sv_index_of(string_view sv, char c) {
  int i = 0;

  while (i < sv.size && sv.data[i] != c) {
    i += 1;
  }
  
  return i < sv.size ? i : -1;
}

int sv_index_of_sv(string_view sv, string_view c) {
  if (c.size > sv.size) {
    return -1;
  }

  int containsIdx = -1;
  int i = 0;
  int j = 0;
  while (i < sv.size) {
    bool idxF = true;
    
    while (j < c.size) {
      if (j + i >= sv.size) {
        idxF = false;
        break;
      }

      if (sv.data[i + j] != c.data[j]) {
        idxF = false;
        break;
      }
      ++j;
    }

    if (idxF) {
      containsIdx = i;
      break;
    }

    ++i;
  }

  return containsIdx;
}

int sv_last_index_of(string_view sv, char c) {
  int i = 0;

  while (i < sv.size && sv.data[sv.size - i - 1] != c) {
    i += 1;
  }
  
  return i < sv.size ? sv.size - 1 - i : -1;
}

int sv_last_index_of_sv(string_view sv, string_view c) {
  if (c.size > sv.size) {
    return -1;
  }

  int containsIdx = -1;
  int i = sv.size - 1;
  int j = 0;
  while (i >= 0) {
    bool idxF = true;
    
    while (j < c.size) {
      if (j + i >= sv.size) {
        idxF = false;
        break;
      }

      if (sv.data[i + j] != c.data[j]) {
        idxF = false;
        break;
      }
      ++j;
    }

    if (idxF) {
      containsIdx = i;
      break;
    }

    --i;
  }

  return containsIdx;
}

string_view sv_substr(string_view sv, size_t start, size_t count) {
  string_view res = sv_construct(sv.data, sv.size);
  res.data += start;
  res.size = count > sv.size ? sv.size : count;
  return res;
}

bool sv_contains(string_view sv, char c) {
  return sv_index_of(sv, c) >= 0;
}

bool sv_contains_sv(string_view sv, string_view c) {
  return sv_index_of_sv(sv, c) >= 0;
}

void sv_copy(char *dest, string_view sv) {
  memcpy(dest, sv.data, sv.size);
  dest[sv.size] = '\0';
  return;
}

void sv_copy_s(char *dest, string_view sv, size_t size) {
  memcpy(dest, sv.data, size > sv.size ? sv.size : size);
  dest[sv.size] = '\0';
  return;
}

// String Builder from Java

typedef struct string_builder {
  size_t size;
  size_t capacity;
  char *data;
} string_builder;

string_builder *sb_new();

/**
 * Appends the given char to the sequence
*/
void sb_append_c(string_builder *sb, const char c);

/**
 * Appends the given char array to the sequence
*/
void sb_append_ch(string_builder *sb, const char *c);

/**
 * Appends the given substring to the sequence
*/
void sb_append_sv(string_builder *sb, string_view sv);

/**
 * Appends a newline to the sequence
*/
void sb_append_newline(string_builder *sb);

/**
 * Appends the given int value to the sequence
*/
void sb_append_i(string_builder *sb, int value);

/**
 * Appends the given float value to the sequence
*/
void sb_append_f(string_builder *sb, float value);

/**
 * Appends the given double value to the sequence
*/
void sb_append_d(string_builder *sb, double value);

/**
 * Reverses the sequence
*/
void sb_reverse(string_builder *sb);

/**
 * Copy the contents to the destination
*/
void sb_to_str(string_builder *sb, char *dest);

string_builder *sb_new() {
  string_builder* builder = malloc(sizeof(string_builder));
  builder->capacity = 64;
  builder->size = 0;
  builder->data = malloc(sizeof(char) * builder->capacity);
  return builder;
}

static void sb_resize(string_builder *sb, size_t capacity) {
  char *data = realloc(sb->data, sizeof(char) * capacity);
  
  if (data) {
    sb->data = data;
    sb->capacity = capacity;
  }
}

void sb_free(string_builder *sb) {
  free(sb->data);
  free(sb);
}

void sb_append_c(string_builder *sb, const char c) {
  if (sb->size == sb->capacity) {
    sb_resize(sb, sb->capacity * 2);
  }

  sb->data[sb->size] = c;
  ++sb->size;
}

void sb_append_ch(string_builder *sb, const char *c) {
  int len = strlen(c);

  for (int i = 0; i < len; ++i) {
    if (c[i] != '\0') sb_append_c(sb, c[i]);
  }
}

void sb_append_sv(string_builder *sb, string_view sv) {
  int len = sv.size;

  for (int i = 0; i < len; ++i) {
    sb_append_c(sb, sv.data[i]);
  }
}

void sb_append_newline(string_builder *sb) {
  sb_append_c(sb, '\n');
}

void sb_append_i(string_builder *sb, int value) {
  char temp[32];
  snprintf(temp, 32, "%d", value);
  char *temp1 = malloc(strlen(temp) + 1);
  memcpy(temp1, temp, strlen(temp) + 1);
  sb_append_ch(sb, temp1);
  free(temp1);
}

void sb_append_f(string_builder *sb, float value) {
  int len = snprintf(NULL, 0, "%f", value);
  char *result = malloc(len + 1);
  snprintf(result, len + 1, "%f", value);
  sb_append_ch(sb, result);
  free(result);
}

void sb_append_d(string_builder *sb, double value) {
  int len = snprintf(NULL, 0, "%f", value);
  char *result = malloc(len + 1);
  snprintf(result, len + 1, "%f", value);
  sb_append_ch(sb, result);
  free(result);
}

void sb_reverse(string_builder *sb) {
  int len = sb->size;

  if (len == 0) return;

  char *temp = malloc(sb->capacity);
  strcpy(temp, sb->data);

  for (int i = 0; i < len; ++i) {
    sb->data[i] = temp[len - 1 - i];
  }

  free(temp);
}

void sb_to_str(string_builder *sb, char *dest) {
  memcpy(dest, sb->data, sb->size);
  dest[sb->size] = '\0';
}

// Pair

#define pair_new_type(type0, type1) typedef struct { type0 first; type1 second; } pair_ ## type0 ##_ ## type1; \

// Math stuff

#define m_min(a, b) (a < b ? a : b)
#define m_max(a, b) (a < b ? b : a)
#define m_clamp(v, n, m) (v < n ? n : v > m ? m : v)
#define m_abs(a) (a < 0 ? -a : a)
#define m_sign(a) (a < 0 ? -1 : 1)

float m_lerp_f(float a, float b, float t) {
  return a;
}

double m_lerp_d(double a, double b, double t) {
  return a;
}

#if __STDC_VERSION__ >= 201112L

#define m_lerp(a, b, t) _Generic((a), \
  float: m_lerp_f(a, b, t), \
  double: m_lerp_d(a, b, t) \
) \

#define M_PI 3.14159
#define M_E 2.718

#endif

#undef KM_SAFE_ITER_INDEX_REACH

#endif // KM_HELPER_H_