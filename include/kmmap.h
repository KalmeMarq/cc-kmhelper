#ifndef KM_MAP_H_INCLUDED
#define KM_MAP_H_INCLUDED

#include <string.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct basic_map_entry {
    char *key;
    void *element;
    struct basic_map_entry *previous;
    struct basic_map_entry *next;
} basic_map_entry;

typedef struct basic_map {
    basic_map_entry *first;
    size_t size;
} basic_map;

bool basic_map_has(basic_map *map, char *key);

basic_map *basic_map_new() {
    basic_map *map = (basic_map *)malloc(sizeof(basic_map));
    map->first = NULL;
    map->size = 0;
    return map;
}

void basic_map_put(basic_map *map, char *key, void *value) {
    if (basic_map_has(map, key)) return;

    struct basic_map_entry *entry = (struct basic_map_entry *)malloc(sizeof(struct basic_map_entry));
    entry->element = value;
    entry->key = (char *)malloc(strlen(key) + 1);
    memcpy(entry->key, key, strlen(key));
    entry->key[strlen(key)] = '\0';
    entry->next = map->first;
    entry->previous = NULL;

    map->first->previous = entry;
    map->first = entry;

    ++map->size;
}

void *basic_map_get(basic_map *map, char *key) {
    for (struct basic_map_entry *entry = (struct basic_map_entry *)map->first; entry != NULL; entry = entry->next) {
        if (strcmp(entry->key, key) == 0) {
            return entry->element;
        }
    }

    return NULL;
}

void basic_map_remove(basic_map *map, char *key) {
    for (struct basic_map_entry *entry = (struct basic_map_entry *)map->first; entry != NULL; entry = entry->next) {
        if (strcmp(entry->key, key) == 0) {
            if (entry->previous != NULL) {
                entry->previous->next = entry->next;
            }

            if (entry->next != NULL) {
                entry->next->previous = entry->previous;
            }

            free(entry->key);
            free(entry->element);
            free(entry);

            --map->size;
            break;
        }
    }
}

void basic_map_clear(basic_map *map) {
    if (map == NULL || map->size == 0) return;

    struct basic_map_entry *curr_entry = map->first;

    for (int i = 0; i < map->size; ++i) {
        free(curr_entry->element);
        curr_entry = curr_entry->next;
    }
}

bool basic_map_has(basic_map *map, char *key) {
    if (map == NULL || map->size == 0) return false;

    struct basic_map_entry *curr_entry = map->first;

    while (curr_entry != NULL) {
        if (strcmp(key, curr_entry->key) == 0) {
            return true;
        }

        curr_entry = curr_entry->next;
    }

    return false;
}

bool basic_map_is_empty(basic_map *map) {
    return map->size == 0;
}

void basic_map_free(basic_map *map) {
    
    struct basic_map_entry *curr_entry = map->first;

    for (int i = 0; i < map->size; ++i) {
        free(curr_entry->element);
        curr_entry = curr_entry->next;
    }
    free(map);
}

// TODO: Hash Map

#endif // KM_MAP_H_INCLUDED