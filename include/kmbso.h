#ifndef KM_BSO_H_INCLUDED
#define KM_BSO_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include "kmbinary.h"
#include "kmstringbuilder.h"

/**
 * Byte
 * ID 0x01
 * 
 * AD
 * Unsigned 0x40
 * Boolean False 0x80
 * Boolean True 0x90
*/
#define BSO_BYTE_TAG_ID 0x01
/**
 * Short
 * ID 0x02
 * 
 * AD
 * Short (as 1 byte) 0x10
 * Short (as 2 byte) 0x00
 * Unsigned (as 1 byte) 0x50
 * Unsigned (as 2 byte) 0x40
*/
#define BSO_SHORT_TAG_ID 0x02
/**
 * Int
 * ID 0x03
 * 
 * AD
 * Int (as 1 byte) 0x20 
 * Int (as 2 byte) 0x10 
 * Int (as 4 byte) 0x00 
 * Unsigned (as 1 byte) 0x60
 * Unsigned (as 2 byte) 0x50
 * Unsigned (as 4 byte) 0x40
*/
#define BSO_INT_TAG_ID 0x03
/**
 * Long
 * ID 0x04
 * 
 * AD
 * Long (as 1 byte) 0x30 
 * Long (as 2 byte) 0x20 
 * Long (as 4 byte) 0x10 
 * Long (as 8 byte) 0x00 
 * Unsigned (as 1 byte) 0x70
 * Unsigned (as 2 byte) 0x60
 * Unsigned (as 4 byte) 0x50
 * Unsigned (as 8 byte) 0x50
*/
#define BSO_LONG_TAG_ID 0x04
/**
 * Float
 * ID 0x05
 * 
 * AD
 * Unsigned 0x40
*/
#define BSO_FLOAT_TAG_ID 0x05
/**
 * Double
 * ID 0x06
 * 
 * AD
 * Unsigned 0x40
*/
#define BSO_DOUBLE_TAG_ID 0x06
/**
 * String
 * ID 0x07
 * 
 * AD
 * None
 * 
 * EXTREME AD
 * ubyte length 0x20
 * int length 0x10
*/
#define BSO_STRING_TAG_ID 0x07
/**
 * Map
 * ID 0x08
 * 
 * AD
 * 1 Byte Length 0x20
 * 2 Byte Length 0x10
 * 4 Byte Length 0x00
*/
#define BSO_MAP_TAG_ID 0x08
/**
 * List
 * ID 0x09
 * 
 * AD
 * 1 Byte Length 0x20
 * 2 Byte Length 0x10
 * 4 Byte Length 0x00
*/
#define BSO_LIST_TAG_ID 0x09
/**
 * Byte Array
 * ID 0x0A
 * 
 * AD
 * 1 Byte Length 0x20
 * 2 Byte Length 0x10
 * 4 Byte Length 0x00
*/
#define BSO_BYTE_ARRAY_TAG_ID 0x0A
/**
 * Short Array
 * ID 0x0B
 * 
 * AD
 * (1 Byte Length) 0x20
 * (2 Byte Length) 0x10
 * (4 Byte Length) 0x00
 * as 1 byte (1 Byte Length) 0x60
 * as 1 byte (2 Byte Length) 0x50
 * as 1 byte (4 Byte Length) 0x40
 * Unsigned (1 Byte Length) 0xE0
 * Unsigned (2 Byte Length) 0xD0
 * Unsigned (4 Byte Length) 0xC0
*/
#define BSO_SHORT_ARRAY_TAG_ID 0x0B
/**
 * Int Array
 * ID 0x0C
 * 
 * AD
 * (1 Byte Length) 0x20
 * (2 Byte Length) 0x10
 * (4 Byte Length) 0x00
 * as 1 byte (1 Byte Length) 0xA0
 * as 1 byte (2 Byte Length) 0x90
 * as 1 byte (4 Byte Length) 0x80
 * as 2 byte (1 Byte Length) 0x60
 * as 2 byte (2 Byte Length) 0x50
 * as 2 byte (4 Byte Length) 0x40
 * Unsigned (1 Byte Length) 0xE0
 * Unsigned (2 Byte Length) 0xD0
 * Unsigned (4 Byte Length) 0xC0
*/
#define BSO_INT_ARRAY_TAG_ID 0x0C
/**
 * Long Array
 * ID 0x0D
 * 
 * AD
 * 1 Byte Length 0x20
 * 2 Byte Length 0x10
 * 4 Byte Length 0x00
 * as 2 byte (1 Byte Length) 0xA0
 * as 2 byte (2 Byte Length) 0x90
 * as 2 byte (4 Byte Length) 0x80
 * as 4 byte (1 Byte Length) 0x60
 * as 4 byte (2 Byte Length) 0x50
 * as 4 byte (4 Byte Length) 0x40
 * Unsigned (1 Byte Length) 0xE0
 * Unsigned (2 Byte Length) 0xD0
 * Unsigned (4 Byte Length) 0xC0
*/
#define BSO_LONG_ARRAY_TAG_ID 0x0D
/**
 * Float Array
 * ID 0x0E
 * 
 * AD
 * 1 Byte Length 0x20
 * 2 Byte Length 0x10
 * 4 Byte Length 0x00
*/
#define BSO_FLOAT_ARRAY_TAG_ID 0x0E
/**
 * Double Array
 * ID 0x0F
 * 
 * AD
 * 1 Byte Length 0x20
 * 2 Byte Length 0x10
 * 4 Byte Length 0x00
*/
#define BSO_DOUBLE_ARRAY_TAG_ID 0x0F

/**
 * Writes SBSO in a compact
*/
#define BSO_SBSO_NO_FORMAT 0x00
/**
 * Writes SBSO with newlines and indentation
*/
#define BSO_SBSO_NEWLINE 0x02
/**
 * Writes SBSO with spaces instead of being compact. Ignored if BSO_SBSO_NEWLINE is in the mask
*/
#define BSO_SBSO_SPACED 0x04
/**
 * Writes SBSO with ansi colors (terminal)
*/
#define BSO_SBSO_ANSI 0x08

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

enum bso_compression {
    NONE,
    GZIP,
    ZLIB
};

/**
 * Changes how to binary data is written
 * 
 * DEFAULT - AD for lengths, int byte length, unsigned and boolean
 * MINIMAL_AD - AD with only unsigned and boolean
 * EXTREME_AD - AD also for string lengths, int type arrays can store values with less bytes
*/
enum bso_binary_representation {
    DEFAULT,
    MINIMAL_AD,
    EXTREME_AD
};

typedef enum bso_type {
    NULLV,
    BYTE,
    SHORT,
    INT,
    LONG,
    FLOAT,
    DOUBLE,
    STRING,
    MAP,
    LIST,
    BYTE_ARRAY,
    SHORT_ARRAY,
    INT_ARRAY,
    LONG_ARRAY,
    FLOAT_ARRAY,
    DOUBLE_ARRAY
} bso_type;

typedef struct bso {
    bso_type type;
    union {
        struct {
            bool is_boolean;
            bool is_unsigned;
            union {
                int8_t value;
                uint8_t uvalue;
            };
        } tag_byte;
        struct {
            bool is_unsigned;
            union {
                int16_t value;
                uint16_t uvalue;
            };
        } tag_short;
        struct {
            bool is_unsigned;
            union {
                int32_t value;
                uint32_t uvalue;
            };
        } tag_int;
        struct {
            bool is_unsigned;
            union {
                int64_t value;
                uint64_t uvalue;
            };
        } tag_long;
        struct {
            float value;
        } tag_float;
        struct {
            float value;
        } tag_double;
        struct {
            char* value;
        } tag_string;
        struct {
            struct bso_simple_map_entry **entries;
            size_t size;
            size_t capacity;
        } tag_map;
        struct {
            void **items;
            size_t size;
            size_t capacity;
        } tag_list;
        struct {
            int8_t *value;
            size_t size;
            size_t capacity;
        } tag_byte_array;
        struct {
            int16_t *value;
            size_t size;
            size_t capacity;
        } tag_short_array;
        struct {
            int32_t *value;
            size_t size;
            size_t capacity;
        } tag_int_array;
        struct {
            int64_t *value;
            size_t size;
            size_t capacity;
        } tag_long_array;
        struct {
            float *value;
            size_t size;
            size_t capacity;
        } tag_float_array;
        struct {
            double *value;
            size_t size;
            size_t capacity;
        } tag_double_array;
    };
} bso;

struct bso_simple_map_entry {
    char *key;
    bso *value;
};

/**
 * Creates a new byte tag (1 byte). If it's part of a map or list freeing it is not needed otherwise it must be freed using bso_free function
*/
bso *bso_new_byte_tag(int8_t value);
/**
 * Creates a new byte tag with unsigned value. If it's part of a map or list freeing it is not needed otherwise it must be freed using bso_free function
*/
bso *bso_new_ubyte_tag(uint8_t value);
/**
 * Creates a new byte tag with a boolean value. If it's part of a map or list freeing it is not needed otherwise it must be freed using bso_free function
*/
bso *bso_new_boolean_tag(bool value);
/**
 * Creates a new short tag (2 bytes). If it's part of a map or list freeing it is not needed otherwise it must be freed using bso_free function
*/
bso *bso_new_short_tag(int16_t value);
/**
 * Creates a new short tag with unsigned value. If it's part of a map or list freeing it is not needed otherwise it must be freed using bso_free function
*/
bso *bso_new_ushort_tag(uint16_t value);
/**
 * Creates a new int tag (4 bytes). If it's part of a map or list freeing it is not needed otherwise it must be freed using bso_free function
*/
bso *bso_new_int_tag(int32_t value);
/**
 * Creates a new int tag with unsigned value. If it's part of a map or list freeing it is not needed otherwise it must be freed using bso_free function
*/
bso *bso_new_uint_tag(uint32_t value);
/**
 * Creates a new long tag (8 bytes). If it's part of a map or list freeing it is not needed otherwise it must be freed using bso_free function
*/
bso *bso_new_long_tag(int64_t value);
/**
 * Creates a new long tag with unsigned value. If it's part of a map or list freeing it is not needed otherwise it must be freed using bso_free function
*/
bso *bso_new_ulong_tag(uint64_t value);
/**
 * Creates a new float tag (4 bytes). If it's part of a map or list freeing it is not needed otherwise it must be freed using bso_free function
*/
bso *bso_new_float_tag(float value);
/**
 * Creates a new double tag (8 bytes). If it's part of a map or list freeing it is not needed otherwise it must be freed using bso_free function
*/
bso *bso_new_double_tag(double value);
/**
 * Creates a new string tag. If it's part of a map or list freeing it is not needed otherwise it must be freed using bso_free function
*/
bso *bso_new_string_tag(char *value, size_t size);
/**
 * Creates a new map tag. Remember to free it using bso_free function
*/
bso *bso_new_map_tag();
/**
 * Puts a bso tag into a map using the given key.
*/
void bso_map_put(bso *map, char *key, bso *value);
/**
 * Gets the bso tag value stored using the given key. If there's no value with the given key it will return NULL
*/
bso *bso_map_get(bso *map, char *key);
/**
 * Clears all the entries inside the map. It frees the bso tag values
*/
void bso_map_clear(bso *map);
/**
 * Creates a new list tag. Remember to free it using bso_free function
*/
bso *bso_new_list_tag();
/**
 * Creates a new list tag with the givens items. Remember to free it using bso_free function
*/
bso *bso_new_list_tag_with_items(bso** items, size_t size);
/**
 * Adds a bso tag to the end of the list
*/
void bso_list_append(bso *list, bso *value);
/**
 * Adds a bso tag at the start of the list
*/
void bso_list_prepend(bso *list, bso *value);
/**
 * Gets a bso tag to the specified index or NULL if index is out of bounds
*/
bso *bso_list_get(bso *list, int index);
/**
 * Gets the bso tag at the start of the list or NULL if list is empty 
*/
bso *bso_list_first(bso *list);
/**
 * Gets the bso tag at the end of the list or NULL if list is empty 
*/
bso *bso_list_last(bso *list);
/**
 * Removes the first bso tag from the list and frees it
*/
void bso_pop_first(bso *list);
/**
 * Removes the last bso tag from the list and frees it
*/
void bso_pop_last(bso *list);
/**
 * Clears all the values in the list and also frees them
*/
void bso_list_clear(bso *list);
/**
 * Remove a value at the specified index
*/
void bso_list_remove(bso *list, size_t index);
/**
 * Changes the value at the specified index
*/
void bso_list_set(bso *list, bso *tag, size_t index);
/**
 * Inserts the value at the specified index
*/
void bso_list_insert(bso *list, bso *tag, size_t index);
/**
 * Creates a new byte array tag (1 byte values). Remember to free it using bso_free function
*/
bso *bso_new_byte_array_tag(int8_t *bytes, size_t size);
/**
 * Creates a new short array tag (2 byte values). Remember to free it using bso_free function
*/
bso *bso_new_short_array_tag(int16_t *bytes, size_t size);
/**
 * Creates a new int array tag (4 byte values). Remember to free it using bso_free function
*/
bso *bso_new_int_array_tag(int32_t *bytes, size_t size);
/**
 * Creates a new long array tag (8 byte values). Remember to free it using bso_free function
*/
bso *bso_new_long_array_tag(int64_t *bytes, size_t size);
/**
 * Creates a new float array tag (4 byte values). Remember to free it using bso_free function
*/
bso *bso_new_float_array_tag(float *bytes, size_t size);
/**
 * Creates a new double array tag (8 byte values). Remember to free it using bso_free function
*/
bso *bso_new_double_array_tag(double *bytes, size_t size);
/**
 * Used to free memory used by bso tags
*/
void bso_free(bso *bso);
/**
 * Parses a bso tag from a file. Remember to free the tag using bso_free function
*/
bso *bso_parse_from_file(FILE *file);
/**
 * Represents the BSO in a string format
*/
char *bso_write_as_sbso(bso *tag, int format);
/**
 * Writes a bso tag into a file
 * @param filepath path to the file
 * @param tag bso tag
 * @param compression if file should be compressed with gzip or zlib algorithms or without compression
 * @param repres how much BSO will try to use its features (additional data) to compress the binary data
*/
void bso_write_to_file(const char *filepath, bso *tag, enum bso_compression compression, enum bso_binary_representation repres);

bso *bso_new_byte_tag(int8_t value) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = BYTE;
    tag->tag_byte.is_boolean = false;
    tag->tag_byte.is_unsigned = false;
    tag->tag_byte.value = value;
    return tag;
}

int8_t bso_byte_value(bso *byte) {
    return byte->tag_byte.value;
}

bso *bso_new_ubyte_tag(uint8_t value) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = BYTE;
    tag->tag_byte.is_boolean = false;
    tag->tag_byte.is_unsigned = true;
    tag->tag_byte.uvalue = value;
    return tag;
}

bso *bso_new_boolean_tag(bool value) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = BYTE;
    tag->tag_byte.is_boolean = true;
    tag->tag_byte.is_unsigned = false;
    tag->tag_byte.value = value;
    return tag;
}

bso *bso_new_short_tag(int16_t value) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = SHORT;
    tag->tag_short.is_unsigned = false;
    tag->tag_short.value = value;
    return tag;
}

bso *bso_new_ushort_tag(uint16_t value) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = SHORT;
    tag->tag_short.is_unsigned = true;
    tag->tag_short.uvalue = value;
    return tag;
}

bso *bso_new_int_tag(int32_t value) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = INT;
    tag->tag_int.is_unsigned = false;
    tag->tag_int.value = value;
    return tag;
}

bso *bso_new_uint_tag(uint32_t value) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = INT;
    tag->tag_int.is_unsigned = true;
    tag->tag_int.uvalue = value;
    return tag;
}

bso *bso_new_long_tag(int64_t value) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = LONG;
    tag->tag_long.is_unsigned = false;
    tag->tag_long.value = value;
    return tag;
}

bso *bso_new_ulong_tag(uint64_t value) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = LONG;
    tag->tag_long.is_unsigned = true;
    tag->tag_long.uvalue = value;
    return tag;
}

bso *bso_new_float_tag(float value) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = FLOAT;
    tag->tag_float.value = value;
    return tag;
}

bso *bso_new_double_tag(double value) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = DOUBLE;
    tag->tag_double.value = value;
    return tag;
}

bso *bso_new_string_tag(char *value, size_t size) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = STRING;
    tag->tag_string.value = (char *)malloc(size + 1);
    memcpy(tag->tag_string.value, value, size);
    tag->tag_string.value[size] = '\0';
    return tag;
}

bso *bso_new_map_tag() {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = MAP;
    tag->tag_map.capacity = 16;
    tag->tag_map.size = 0;
    tag->tag_map.entries = (struct bso_simple_map_entry **)malloc(sizeof(struct bso_simple_map_entry) * tag->tag_map.capacity);
    return tag;
}

void bso_map_put(bso *map, char *key, bso *value) {
    bool wasfound = false;
    for (size_t i = 0; i < map->tag_map.size; ++i) {
        if (strcmp(map->tag_map.entries[i]->key, key) == 0) {
            bso_free(map->tag_map.entries[i]->value);
            map->tag_map.entries[i]->value = value;
            wasfound = true;
            break;
        }
    }

    if (!wasfound) {
        if (map->tag_map.size == map->tag_map.capacity) {
            void **nptr = (void **)realloc(map->tag_map.entries, sizeof(struct bso_simple_map_entry) * map->tag_map.capacity * 2);
            map->tag_map.capacity *= 2;
            map->tag_map.entries = (struct bso_simple_map_entry **)nptr;
        }

        struct bso_simple_map_entry *entry = (struct bso_simple_map_entry *)malloc(sizeof(struct bso_simple_map_entry));
        int len = strlen(key);
        entry->key = (char *)malloc(len + 1);
        for (int i = 0; i < len; ++i) {
            entry->key[i] = key[i];
        }
        entry->key[len] = '\0';
        entry->value = value;
        map->tag_map.entries[map->tag_map.size++] = entry;
    }
}

bso *bso_map_get(bso *map, char *key) {
    for (size_t i = 0; i < map->tag_map.size; ++i) {
        if (strcmp(map->tag_map.entries[i]->key, key) == 0) {
            return map->tag_map.entries[i]->value;
        }
    }
    
    return NULL;
}

void bso_map_clear(bso *map) {
    for (size_t i = 0; i < map->tag_map.size; ++i) {
        bso_free(map->tag_map.entries[i]->value);
        free(map->tag_map.entries[i]->key);
        free(map->tag_map.entries[i]);
    }
    map->tag_map.size = 0;
}

bso *bso_new_list_tag() {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = LIST;
    tag->tag_list.capacity = 16;
    tag->tag_list.items = (void **)malloc(sizeof(void *) * tag->tag_list.capacity);
    tag->tag_list.size = 0;
    return tag;
}

bso *bso_new_list_tag_with_items(bso** items, size_t size) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = LIST;
    tag->tag_list.capacity = size * 2;
    tag->tag_list.items = (void **)malloc(sizeof(void *) * tag->tag_list.capacity);
    for (size_t i = 0; i < size; ++i) {
        tag->tag_list.items[i] = items[i];
    }
    tag->tag_list.size = size;
    return tag;
}

void bso_list_append(bso *list, bso *value) {
    if (list == NULL || list->type != LIST) return;

    if (list->tag_list.size == list->tag_list.capacity) {
        void **newptr = (void**)realloc(list->tag_list.items, list->tag_list.capacity * 2);
        list->tag_list.capacity *= 2;
        list->tag_list.items = newptr;
    }

    list->tag_list.items[list->tag_list.size++] = value;
}

void bso_list_prepend(bso *list, bso *value) {
    if (list == NULL || list->type != LIST) return;

    if (list->tag_list.size == list->tag_list.capacity) {
        void **newptr = (void**)realloc(list->tag_list.items, list->tag_list.capacity * 2);
        list->tag_list.capacity *= 2;
        list->tag_list.items = newptr;
    }

    for (int i = list->tag_list.size - 1; i >= 0; --i) {
        list->tag_list.items[i + 1] = list->tag_list.items[i];
    }

    list->tag_list.items[0] = value;
    list->tag_list.size++;
}

bso *bso_list_get(bso *list, int index) {
    if (list == NULL || list->type != LIST || list->tag_list.size == 0 || index < 0 || index >= (int)list->tag_list.size) return NULL;
    return (bso *)list->tag_list.items[index];
}

bso *bso_list_first(bso *list) {
    if (list == NULL || list->type != LIST || list->tag_list.size == 0) return NULL;
    return (bso *)list->tag_list.items[0];
}

bso *bso_list_last(bso *list) {
    if (list == NULL || list->type != LIST || list->tag_list.size == 0) return NULL;
    return (bso *)list->tag_list.items[list->tag_list.size - 1];
}

void bso_pop_first(bso *list) {
    if (list == NULL || list->type != LIST || list->tag_list.size == 0) return;

    bso_free((bso *)list->tag_list.items[0]);
    --list->tag_list.size;

    for (size_t i = 1; i <= list->tag_list.size; ++i) {
        list->tag_list.items[i - 1] = list->tag_list.items[i];
    }
}

void bso_pop_last(bso *list) {
    if (list == NULL || list->type != LIST || list->tag_list.size == 0) return;
    bso_free((bso *)list->tag_list.items[list->tag_list.size - 1]);
    --list->tag_list.size;
}

void bso_list_clear(bso *list) {
    if (list == NULL || list->type != LIST || list->tag_list.size == 0) return;

    for (size_t i = 0; i < list->tag_list.size; ++i) {
        bso_free((bso *) list->tag_list.items[i]);
    }

    list->tag_list.size = 0;
}

void bso_list_remove(bso *list, size_t index) {
    if (list == NULL || list->type != LIST || list->tag_list.size == 0 || index >= list->tag_list.size) return;

    bso_free((bso *)list->tag_list.items[index]);

    for (size_t i = index + 1; i < list->tag_list.size; ++i) {
        list->tag_list.items[i - 1] = list->tag_list.items[i];
    }
    
    --list->tag_list.size;
}

void bso_list_set(bso *list, bso *tag, size_t index) {
    if (list == NULL || list->type != LIST || list->tag_list.size == 0 || index >= list->tag_list.size) return;

    bso_free((bso *)list->tag_list.items[index]);
    list->tag_list.items[index] = tag;
}

void bso_list_insert(bso *list, bso *tag, size_t index) {
    if (list == NULL || list->type != LIST || list->tag_list.size == 0 || index > list->tag_list.size) return;

    if (list->tag_list.size == list->tag_list.capacity) {
        void **newptr = (void**)realloc(list->tag_list.items, list->tag_list.capacity * 2);
        list->tag_list.capacity *= 2;
        list->tag_list.items = newptr;
    }

    for (size_t i = index; i < list->tag_list.size; ++i) {
        list->tag_list.items[i + 1] = list->tag_list.items[i];
    }

    list->tag_list.items[index] = tag;
    ++list->tag_list.size;
}

bso *bso_new_byte_array_tag(int8_t *bytes, size_t size) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = BYTE_ARRAY;
    tag->tag_byte_array.size = size;
    tag->tag_byte_array.value = (int8_t *)malloc(size);
    memcpy(tag->tag_byte_array.value, bytes, size);
    return tag;
}

bso *bso_new_short_array_tag(int16_t *bytes, size_t size) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = SHORT_ARRAY;
    tag->tag_short_array.size = size;
    tag->tag_short_array.value = (int16_t *)malloc(size * sizeof(int16_t));
    for (size_t i = 0; i < size; ++i) {
        tag->tag_short_array.value[i] = bytes[i];
    }
    return tag;
}

bso *bso_new_int_array_tag(int32_t *bytes, size_t size) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = INT_ARRAY;
    tag->tag_int_array.size = size;
    tag->tag_int_array.value = (int32_t *)malloc(size * sizeof(int32_t));
    for (size_t i = 0; i < size; ++i) {
        tag->tag_int_array.value[i] = bytes[i];
    }
    return tag;
}

bso *bso_new_long_array_tag(int64_t *bytes, size_t size) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = LONG_ARRAY;
    tag->tag_long_array.size = size;
    tag->tag_long_array.value = (int64_t *)malloc(size * sizeof(int64_t));
    for (size_t i = 0; i < size; ++i) {
        tag->tag_long_array.value[i] = bytes[i];
    }
    return tag;
}

bso *bso_new_float_array_tag(float *bytes, size_t size) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = FLOAT_ARRAY;
    tag->tag_float_array.size = size;
    tag->tag_float_array.value = (float *)malloc(size);
    memcpy(tag->tag_float_array.value, bytes, size);
    return tag;
}

bso *bso_new_double_array_tag(double *bytes, size_t size) {
    bso *tag = (bso *)malloc(sizeof(bso));
    tag->type = DOUBLE_ARRAY;
    tag->tag_double_array.size = size;
    tag->tag_double_array.value = (double *)malloc(size);
    memcpy(tag->tag_double_array.value, bytes, size);
    return tag;
}

void bso_free(bso *bso) {
    if (bso->type == STRING) {
        free(bso->tag_string.value);
    } else if (bso->type == BYTE_ARRAY) {
        free(bso->tag_byte_array.value);
    } else if (bso->type == LIST) {
        for (size_t i = bso->tag_list.size - 1; i > 0; --i) {
            free(bso->tag_list.items[i]);
        }
        free(bso->tag_list.items);
    }
    
    free(bso);
}

bso *bso_parse(uint8_t id, uint8_t adb, binary_reader *reader) {
    bso *tag = NULL;

    if (id == 0x01) {
        if (adb == 0x80) {
            tag = bso_new_boolean_tag(false);
        } else if (adb == 0x90) {
            tag = bso_new_boolean_tag(true);
        } else if (adb == 0x40) {
            tag = bso_new_ubyte_tag((uint8_t) binary_reader_get_int8(reader));
        } else {
            tag = bso_new_byte_tag(binary_reader_get_int8(reader));
        }
    } else if (id == 0x02) {
        if (adb > 0x30) {
            tag = bso_new_ushort_tag((adb & 0x30) == 0x10 ? binary_reader_get_int8(reader) : binary_reader_get_int16(reader));
        } else {
            tag = bso_new_short_tag((adb & 0x30) == 0x10 ? binary_reader_get_int8(reader) : binary_reader_get_int16(reader));
        }
    } else if (id == BSO_INT_TAG_ID) {
        if (adb > 0x30) {
            tag = bso_new_uint_tag((uint32_t) ((adb & 0x30) == 0x10 ? binary_reader_get_int16(reader) : (adb & 0x30) == 0x20 ? binary_reader_get_int8(reader) : binary_reader_get_int32(reader)));
        } else {
            tag = bso_new_int_tag((adb & 0x30) == 0x10 ? binary_reader_get_int16(reader) : (adb & 0x30) == 0x20 ? binary_reader_get_int8(reader) : binary_reader_get_int32(reader));
        }
    } else if (id == BSO_LONG_TAG_ID) {
        if (adb > 0x30) {
            tag = bso_new_ulong_tag((adb & 0x30) == 0x10 ? binary_reader_get_int32(reader) : (adb & 0x30) == 0x20 ? binary_reader_get_int16(reader) : (adb & 0x30) == 0x30 ? binary_reader_get_int8(reader) : binary_reader_get_int64(reader));
        } else {
            tag = bso_new_long_tag((adb & 0x30) == 0x10 ? binary_reader_get_int32(reader) : (adb & 0x30) == 0x20 ? binary_reader_get_int16(reader) : (adb & 0x30) == 0x30 ? binary_reader_get_int8(reader) : binary_reader_get_int64(reader));
        }
    } else if (id == 0x07) {
        size_t len = binary_reader_get_int16(reader);
        char *text = (char *)malloc(len + 1);
        for (size_t i = 0; i < len; ++i) {
            text[i] = binary_reader_get_int8(reader);
        }
        text[len] = '\0';
        tag = bso_new_string_tag(text, len);
        free(text);
    } else if (id == BSO_MAP_TAG_ID) {
        size_t len = 0;
        if (adb == 0x20) {
            len = (uint8_t) binary_reader_get_int8(reader);
        } else if (adb == 0x10) {
            len = (uint16_t) binary_reader_get_int16(reader);
        } else {
            len = binary_reader_get_int32(reader);
        }

        bso *map = bso_new_map_tag();

        for (size_t i = 0; i < len; ++i) {
            char *k = binary_reader_get_string(reader);
            int _id = binary_reader_get_int8(reader);
            int _adb = _id & 0xF0;
            _id = _id & 0x0F;
            bso *t = bso_parse(_id, _adb, reader);
            bso_map_put(map, k, t);
            free(k);
        }

        tag = map;
    } else if (id == 0x0A) {
        size_t len = 0;
        if (adb == 0x20) {
            len = (uint8_t) binary_reader_get_int8(reader);
        } else if (adb == 0x10) {
            len = (uint16_t) binary_reader_get_int16(reader);
        } else {
            len = binary_reader_get_int32(reader);
        }
        int8_t *bytes = (int8_t *)malloc(len);
        for (size_t i = 0; i < len; ++i) {
            bytes[i] = binary_reader_get_int8(reader);
        }
        tag = bso_new_byte_array_tag(bytes, len);
        free(bytes);
    } else if (id == 0x0B) {
        size_t len = 0;
        if (adb == 0x20) {
            len = (uint8_t) binary_reader_get_int8(reader);
        } else if (adb == 0x10) {
            len = (uint16_t) binary_reader_get_int16(reader);
        } else {
            len = binary_reader_get_int32(reader);
        }
        int16_t *bytes = (int16_t *)malloc(len);
        for (size_t i = 0; i < len; ++i) {
            bytes[i] = binary_reader_get_int16(reader);
        }
        tag = bso_new_short_array_tag(bytes, len);
        free(bytes);
    } else if (id == 0x0C) {
        size_t len = 0;
        if (adb == 0x20) {
            len = (uint8_t) binary_reader_get_int8(reader);
        } else if (adb == 0x10) {
            len = (uint16_t) binary_reader_get_int16(reader);
        } else {
            len = binary_reader_get_int32(reader);
        }
        int32_t *bytes = (int32_t *)malloc(len);
        for (size_t i = 0; i < len; ++i) {
            bytes[i] = binary_reader_get_int32(reader);
        }
        tag = bso_new_int_array_tag(bytes, len);
        free(bytes);
    } else if (id == 0x0D) {
        size_t len = 0;
        if (adb == 0x20) {
            len = (uint8_t) binary_reader_get_int8(reader);
        } else if (adb == 0x10) {
            len = (uint16_t) binary_reader_get_int16(reader);
        } else {
            len = binary_reader_get_int32(reader);
        }
        int64_t *bytes = (int64_t *)malloc(len);
        for (size_t i = 0; i < len; ++i) {
            bytes[i] = binary_reader_get_int64(reader);
        }
        tag = bso_new_long_array_tag(bytes, len);
        free(bytes);
    } else if (id == 0x0E) {
        size_t len = 0;
        if (adb == 0x20) {
            len = (uint8_t) binary_reader_get_int8(reader);
        } else if (adb == 0x10) {
            len = (uint16_t) binary_reader_get_int16(reader);
        } else {
            len = binary_reader_get_int32(reader);
        }
        float *bytes = (float *)malloc(len);
        for (size_t i = 0; i < len; ++i) {
            bytes[i] = binary_reader_get_float32(reader);
        }
        tag = bso_new_float_array_tag(bytes, len);
        free(bytes);
    } else if (id == 0x0F) {
        size_t len = 0;
        if (adb == 0x20) {
            len = (uint8_t) binary_reader_get_int8(reader);
        } else if (adb == 0x10) {
            len = (uint16_t) binary_reader_get_int16(reader);
        } else {
            len = binary_reader_get_int32(reader);
        }
        double *bytes = (double *)malloc(len);
        for (size_t i = 0; i < len; ++i) {
            bytes[i] = binary_reader_get_float64(reader);
        }
        tag = bso_new_double_array_tag(bytes, len);
        free(bytes);
    }
    
    return tag;
}

bso *bso_parse_from_file(FILE *file) {    
    binary_reader *reader = binary_reader_from_file(file);

    if ((reader == NULL) | (reader->size == 0)) return NULL;

    uint8_t b = binary_reader_get_int8(reader);
    uint8_t adb = b & 0xF0;
    b &= 0x0F;
    bso *tag = bso_parse(b, adb, reader);    
    free(reader);

    return tag;
}

bso *bso_parse_from_file_path(const char *filepath) {
    FILE *f = fopen(filepath, "rb");
    bso *bso = bso_parse_from_file(f);
    fclose(f);
    return bso;
}

void bso_write_binary_write(binary_writer *writer, bso *tag, enum bso_binary_representation repres) {
    if (tag->type == BYTE) {
        int idad = 0x01;
        if (tag->tag_byte.is_boolean) {
            idad = tag->tag_byte.value == 0 ? 0x81 : 0x91;
        } else if (tag->tag_byte.is_unsigned) {
            idad = 0x41;
        }

        binary_writer_put_int8(writer, idad);
        
        if (tag->tag_byte.is_unsigned) {
            binary_writer_put_int8(writer, tag->tag_byte.uvalue & 0xFF);
        } else if (!tag->tag_byte.is_boolean) {
            binary_writer_put_int8(writer, tag->tag_byte.value & 0xFF);
        }
    } else if (tag->type == SHORT) {
        if (repres == MINIMAL_AD) {
                binary_writer_put_int8(writer, 0x02 + (tag->tag_short.is_unsigned ? 0x40 : 0x00));
                binary_writer_put_int16(writer, tag->tag_short.value & 0xFFFF);
        } else {
            if (tag->tag_short.value >= -128 && tag->tag_short.value <= 127) {
                binary_writer_put_int8(writer, 0x12 + (tag->tag_short.is_unsigned ? 0x40 : 0x00));
                binary_writer_put_int8(writer, tag->tag_short.value & 0xFF);
            } else {
                binary_writer_put_int8(writer, 0x02 + (tag->tag_short.is_unsigned ? 0x40 : 0x00));
                binary_writer_put_int16(writer, tag->tag_short.value & 0xFFFF);
            }
        }
    } else if (tag->type == INT) {
        if (repres == MINIMAL_AD) {
                binary_writer_put_int8(writer, 0x03 + (tag->tag_int.is_unsigned ? 0x40 : 0x00));
                binary_writer_put_int32(writer, (tag->tag_int.is_unsigned ? tag->tag_int.uvalue : (uint32_t)tag->tag_int.value) & 0xFFFFFFFF);
        } else {
            if (tag->tag_int.value >= -128 && tag->tag_int.value <= 127) {
                binary_writer_put_int8(writer, 0x23 + (tag->tag_int.is_unsigned ? 0x40 : 0x00));
                binary_writer_put_int8(writer, (tag->tag_int.is_unsigned ? tag->tag_int.uvalue : (uint32_t)tag->tag_int.value) & 0xFF);
            } else if (tag->tag_int.value >= -32768 && tag->tag_int.value <= 32767) {
                binary_writer_put_int8(writer, 0x13 + (tag->tag_int.is_unsigned ? 0x40 : 0x00));
                binary_writer_put_int16(writer, (tag->tag_int.is_unsigned ? tag->tag_int.uvalue : (uint32_t)tag->tag_int.value) & 0xFFFF);
            } else {
                binary_writer_put_int8(writer, 0x03 + (tag->tag_int.is_unsigned ? 0x40 : 0x00));
                binary_writer_put_int32(writer, (tag->tag_int.is_unsigned ? tag->tag_int.uvalue : (uint32_t)tag->tag_int.value) & 0xFFFFFFFF);
            }
        }
    } else if (tag->type == LONG) {
        if (repres == MINIMAL_AD) {
            if (tag->tag_long.value >= -128 && tag->tag_long.value <= 127) {
                binary_writer_put_int8(writer, 0x34 + (tag->tag_long.is_unsigned ? 0x40 : 0x00));
                binary_writer_put_int8(writer, tag->tag_long.value & 0xFF);
            } else if (tag->tag_long.value >= -32768 && tag->tag_long.value <= 32767) {
                binary_writer_put_int8(writer, 0x24 + (tag->tag_long.is_unsigned ? 0x40 : 0x00));
                binary_writer_put_int16(writer, tag->tag_long.value & 0xFFFF);
            } else if (tag->tag_long.value >= -2147483648 && tag->tag_long.value <= 2147483647) {
                binary_writer_put_int8(writer, 0x14 + (tag->tag_long.is_unsigned ? 0x40 : 0x00));
                binary_writer_put_int32(writer, tag->tag_long.value & 0xFFFFFFFF);
            } else {
                binary_writer_put_int8(writer, 0x04 + (tag->tag_long.is_unsigned ? 0x40 : 0x00));
                binary_writer_put_int64(writer, tag->tag_long.value);
            }
        } else {
            binary_writer_put_int8(writer, 0x04 + (tag->tag_long.is_unsigned ? 0x40 : 0x00));
            binary_writer_put_int64(writer, tag->tag_long.value);
        }
    } else if (tag->type == STRING) {
        size_t len = strlen(tag->tag_string.value);
        
        if (repres == EXTREME_AD) {
            if (len <= 255) {
                binary_writer_put_int8(writer, 0x27);
                binary_writer_put_int8(writer, len);
            } else if (len > 65535) {
                binary_writer_put_int8(writer, 0x17);
                binary_writer_put_int32(writer, len);
            } else {
                binary_writer_put_int8(writer, 0x07);
                binary_writer_put_int16(writer, len);
            }
        } else {
            binary_writer_put_int8(writer, 0x07);
            binary_writer_put_int16(writer, len);
        }

        for (size_t i = 0; i < len; ++i) {
            binary_writer_put_int8(writer, tag->tag_string.value[i]);
        }
    } else if (tag->type == MAP) {
        if (repres == MINIMAL_AD) {
            binary_writer_put_int8(writer, 0x08);
            binary_writer_put_int32(writer, tag->tag_map.size & 0xFFFFFFFF);
        } else {
            if (tag->tag_map.size >= (size_t) -128 && tag->tag_map.size <= (size_t) 127) {
                binary_writer_put_int8(writer, 0x28);
                binary_writer_put_int8(writer, tag->tag_map.size & 0xFF);
            } else if (tag->tag_map.size >= (size_t) -32768 && tag->tag_map.size <= (size_t) 32767) {
                binary_writer_put_int8(writer, 0x18);
                binary_writer_put_int16(writer, tag->tag_map.size & 0xFF);
            } else {
                binary_writer_put_int8(writer, 0x08);
                binary_writer_put_int32(writer, tag->tag_map.size & 0xFFFFFFFF);
            }
        }

        for (size_t i = 0; i < tag->tag_map.size; ++i) {
            binary_writer_put_string(writer, (uint8_t *)tag->tag_map.entries[i]->key, strlen(tag->tag_map.entries[i]->key));
            bso_write_binary_write(writer, tag->tag_map.entries[i]->value, repres);
        }
    } else if (tag->type == LIST) {
        if (repres == MINIMAL_AD) {
            binary_writer_put_int8(writer, 0x09);
            binary_writer_put_int32(writer, tag->tag_list.size & 0xFFFFFFFF);
        } else {
            if (tag->tag_list.size >= (size_t) -128 && tag->tag_list.size <= (size_t) 127) {
                binary_writer_put_int8(writer, 0x29);
                binary_writer_put_int8(writer, tag->tag_list.size & 0xFF);
            } else if (tag->tag_list.size >= (size_t) -32768 && tag->tag_list.size <= (size_t) 32767) {
                binary_writer_put_int8(writer, 0x19);
                binary_writer_put_int16(writer, tag->tag_list.size & 0xFF);
            } else {
                binary_writer_put_int8(writer, 0x09);
                binary_writer_put_int32(writer, tag->tag_list.size & 0xFFFFFFFF);
            }
        }

        for (size_t i = 0; i < tag->tag_list.size; ++i) {
            bso_write_binary_write(writer, (bso *)tag->tag_list.items[i], repres);
        }
    } else if (tag->type == BYTE_ARRAY) {
        if (repres == MINIMAL_AD) {
            binary_writer_put_int8(writer, 0x0A);
            binary_writer_put_int32(writer, tag->tag_byte_array.size & 0xFFFFFFFF);
        } else {
            if (tag->tag_byte_array.size >= (size_t) -128 && tag->tag_byte_array.size <= (size_t) 127) {
                binary_writer_put_int8(writer, 0x2A);
                binary_writer_put_int8(writer, tag->tag_byte_array.size & 0xFF);
            } else if (tag->tag_byte_array.size >= (size_t) -32768 && tag->tag_byte_array.size <= (size_t) 32767) {
                binary_writer_put_int8(writer, 0x1A);
                binary_writer_put_int16(writer, tag->tag_byte_array.size & 0xFF);
            } else {
                binary_writer_put_int8(writer, 0x0A);
                binary_writer_put_int32(writer, tag->tag_byte_array.size & 0xFFFFFFFF);
            }
        }

        for (size_t i = 0; i < tag->tag_byte_array.size; ++i) {
            binary_writer_put_int8(writer, tag->tag_byte_array.value[i]);
        }
    } else if (tag->type == SHORT_ARRAY) {
        int add = 0;
        if (repres == MINIMAL_AD) {
            binary_writer_put_int8(writer, 0x0B);
            binary_writer_put_int32(writer, tag->tag_short_array.size & 0xFFFFFFFF);
        } else {
            if (repres == EXTREME_AD) {
                bool outside = false;
                for (size_t i = 0; i < tag->tag_short_array.size; ++i) {
                    if (!(tag->tag_short_array.value[i] >= -128 && tag->tag_short_array.value[i] <= 127)) {
                        outside = true;
                        break;
                    }
                }

                if (!outside) add = 0x40;
            }

            if (tag->tag_short_array.size >= (size_t) -128 && tag->tag_short_array.size <= (size_t) 127) {
                binary_writer_put_int8(writer, 0x2B + add);
                binary_writer_put_int8(writer, tag->tag_short_array.size & 0xFF);
            } else if (tag->tag_short_array.size >= (size_t) -32768 && tag->tag_short_array.size <= (size_t) 32767) {
                binary_writer_put_int8(writer, 0x1B + add);
                binary_writer_put_int16(writer, tag->tag_short_array.size & 0xFF);
            } else {
                binary_writer_put_int8(writer, 0x0B + add);
                binary_writer_put_int32(writer, tag->tag_short_array.size & 0xFFFFFFFF);
            }
        }

        for (size_t i = 0; i < tag->tag_short_array.size; ++i) {
            if (add == 0x40) binary_writer_put_int8(writer, tag->tag_short_array.value[i]);
            else binary_writer_put_int16(writer, tag->tag_short_array.value[i]);
        }
    } else if (tag->type == INT_ARRAY) {
        int add = 0;
        if (repres == MINIMAL_AD) {
            binary_writer_put_int8(writer, 0x0C);
            binary_writer_put_int32(writer, tag->tag_int_array.size & 0xFFFFFFFF); 
        } else {
            if (repres == EXTREME_AD) {
                int32_t min = 0;
                int32_t max = 0;
                for (size_t i = 0; i < tag->tag_int_array.size; ++i) {
                    if (tag->tag_int_array.value[i] < min) {
                        min = tag->tag_int_array.value[i];
                    }

                    if (tag->tag_int_array.value[i] > max) {
                        max = tag->tag_int_array.value[i];
                    }
                }

                if (min >= -128 && max <= 127) {
                    add = 0x80;
                } else if (min >= -32768 && max <= 32767) {
                    add = 0x40;
                }
            }

            if (tag->tag_int_array.size >= (size_t) -128 && tag->tag_int_array.size <= (size_t) 127) {
                binary_writer_put_int8(writer, 0x2C);
                binary_writer_put_int8(writer, tag->tag_int_array.size & 0xFF);
            } else if (tag->tag_int_array.size >= (size_t) -32768 && tag->tag_int_array.size <= (size_t) 32767) {
                binary_writer_put_int8(writer, 0x1C);
                binary_writer_put_int16(writer, tag->tag_int_array.size & 0xFF);
            } else {
                binary_writer_put_int8(writer, 0x0C);
                binary_writer_put_int32(writer, tag->tag_int_array.size & 0xFFFFFFFF);
            }
        }

        for (size_t i = 0; i < tag->tag_int_array.size; ++i) {
            if (add == 0x80) binary_writer_put_int8(writer, tag->tag_int_array.value[i]);
            else if (add == 0x40) binary_writer_put_int16(writer, tag->tag_int_array.value[i]);
            else binary_writer_put_int32(writer, tag->tag_int_array.value[i]);
        }
    } else if (tag->type == LONG_ARRAY) {
        int add = 0;
        if (repres == MINIMAL_AD) {
            binary_writer_put_int8(writer, 0x0D);
            binary_writer_put_int32(writer, tag->tag_long_array.size & 0xFFFFFFFF);
        } else {
            if (tag->tag_long_array.size >= (size_t) -128 && tag->tag_long_array.size <= (size_t) 127) {
                binary_writer_put_int8(writer, 0x2D);
                binary_writer_put_int8(writer, tag->tag_long_array.size & 0xFF);
            } else if (tag->tag_long_array.size >= (size_t) -32768 && tag->tag_long_array.size <= (size_t) 32767) {
                binary_writer_put_int8(writer, 0x1D);
                binary_writer_put_int16(writer, tag->tag_long_array.size & 0xFF);
            } else {
                binary_writer_put_int8(writer, 0x0D);
                binary_writer_put_int32(writer, tag->tag_long_array.size & 0xFFFFFFFF);
            }
        }

        for (size_t i = 0; i < tag->tag_long_array.size; ++i) {
            if (add == 0x80) binary_writer_put_int16(writer, tag->tag_long_array.value[i]);
            else if (add == 0x40) binary_writer_put_int32(writer, tag->tag_long_array.value[i]);
            else binary_writer_put_int64(writer, tag->tag_long_array.value[i]);
        }
    } else if (tag->type == FLOAT_ARRAY) {
        if (repres == MINIMAL_AD) {
            binary_writer_put_int8(writer, 0x0E);
            binary_writer_put_int32(writer, tag->tag_float_array.size & 0xFFFFFFFF);
        } else {
             if (tag->tag_float_array.size >= (size_t) -128 && tag->tag_float_array.size <= (size_t) 127) {
                binary_writer_put_int8(writer, 0x2E);
                binary_writer_put_int8(writer, tag->tag_float_array.size & 0xFF);
            } else if (tag->tag_float_array.size >= (size_t) -32768 && tag->tag_float_array.size <= (size_t) 32767) {
                binary_writer_put_int8(writer, 0x1E);
                binary_writer_put_int16(writer, tag->tag_float_array.size & 0xFF);
            } else {
                binary_writer_put_int8(writer, 0x0E);
                binary_writer_put_int32(writer, tag->tag_float_array.size & 0xFFFFFFFF);
            }
        }

        for (size_t i = 0; i < tag->tag_float_array.size; ++i) {
            binary_writer_put_float32(writer, tag->tag_float_array.value[i]);
        }
    } else if (tag->type == DOUBLE_ARRAY) {
        if (repres == MINIMAL_AD) {
            binary_writer_put_int8(writer, 0x0F);
            binary_writer_put_int32(writer, tag->tag_double_array.size & 0xFFFFFFFF);
        } else {
            if (tag->tag_double_array.size >= (size_t) -128 && tag->tag_double_array.size <= (size_t) 127) {
                binary_writer_put_int8(writer, 0x2F);
                binary_writer_put_int8(writer, tag->tag_double_array.size & 0xFF);
            } else if (tag->tag_double_array.size >= (size_t) -32768 && tag->tag_double_array.size <= (size_t) 32767) {
                binary_writer_put_int8(writer, 0x1F);
                binary_writer_put_int16(writer, tag->tag_double_array.size & 0xFF);
            } else {
                binary_writer_put_int8(writer, 0x0F);
                binary_writer_put_int32(writer, tag->tag_double_array.size & 0xFFFFFFFF);
            }
        }

        for (size_t i = 0; i < tag->tag_double_array.size; ++i) {
            binary_writer_put_float64(writer, tag->tag_double_array.value[i]);
        }
    }
}

void bso_write_to_file(const char *filepath, bso *tag, enum bso_compression compression, enum bso_binary_representation repres) {
    if (tag == NULL || filepath == NULL) return;

    FILE *f = fopen(filepath, "wb");

    binary_writer *writer = binary_writer_new(64);

    bso_write_binary_write(writer, tag, repres);

    fwrite(writer->data, sizeof(uint8_t), writer->size, f);

    binary_writer_free(writer);

    fclose(f);
}

void bso_write_as_sbso_sb(string_builder *sb, bso *tag, bool ansi_colors, bool spaced, bool use_newline, int depth) {
    if (tag == NULL) return;

    if (tag->type == BYTE) {
        if (tag->tag_byte.is_boolean) {
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
            sb_append_ch(sb, tag->tag_byte.value == 0 ? "false" : "true");
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
        } else {
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
            sb_append_i(sb, tag->tag_byte.is_unsigned ? tag->tag_byte.uvalue : tag->tag_byte.value);
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RED);
            if (tag->tag_byte.is_unsigned) sb_append_c(sb, 'u');
            sb_append_c(sb, 'b');
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
        }
    } else if (tag->type == SHORT) {
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
        if (tag->tag_short.is_unsigned) sb_append_i(sb, tag->tag_short.uvalue);
        else sb_append_i(sb, tag->tag_short.value);
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RED);
        if (tag->tag_short.is_unsigned) sb_append_c(sb, 'u');
        sb_append_c(sb, 's');
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
    } else if (tag->type == INT) {
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
        if (tag->tag_int.is_unsigned) sb_append_i(sb, tag->tag_int.uvalue);
        else sb_append_i(sb, tag->tag_int.value);
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RED);
        if (tag->tag_long.is_unsigned) sb_append_c(sb, 'u');
        sb_append_c(sb, 'i');
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
    } else if (tag->type == LONG) {
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
        if (tag->tag_long.is_unsigned) sb_append_i(sb, tag->tag_long.uvalue);
        else sb_append_ll(sb, tag->tag_long.value);
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RED);
        if (tag->tag_long.is_unsigned) sb_append_c(sb, 'U');
        sb_append_c(sb, 'L');
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
    } else if (tag->type == FLOAT) {
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
        sb_append_i(sb, tag->tag_float.value);
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RED);
        sb_append_c(sb, 'f');
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
    } else if (tag->type == DOUBLE) {
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
        sb_append_i(sb, tag->tag_double.value);
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RED);
        sb_append_c(sb, 'd');
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
    } else if (tag->type == STRING) {
        sb_append_c(sb, '"');
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_GREEN);
        sb_append_ch(sb, tag->tag_string.value);
        if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
        sb_append_c(sb, '"');
    } else if (tag->type == MAP) {
        sb_append_c(sb, '{');

        if (use_newline) {
            sb_append_c(sb, '\n');
        } else if (spaced) {
            sb_append_c(sb, ' ');
        }

        for (size_t i = 0; i < tag->tag_map.size; ++i) {
            if (use_newline) {
                for (int i = 0; i < depth + 1; ++i) {
                    sb_append_ch(sb, "  ");
                }
            }

            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_CYAN);
            sb_append_ch(sb, tag->tag_map.entries[i]->key);
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
            sb_append_c(sb, ':');
            if (use_newline || spaced) sb_append_c(sb, ' ');
            bso_write_as_sbso_sb(sb, (bso *)tag->tag_map.entries[i]->value, ansi_colors, spaced, use_newline, depth + 1);
        
             if (i + 1 < tag->tag_map.size) {
                sb_append_c(sb, ',');
                if (use_newline) sb_append_c(sb, '\n');
                else if (spaced) sb_append_c(sb, ' ');
            }
        }

        if (use_newline) {
            sb_append_c(sb, '\n');
        
            for (int i = 0; i < depth; ++i) {
                sb_append_ch(sb, "  ");
            }
        } else if (spaced) {
            sb_append_c(sb, ' ');
        }
        sb_append_c(sb, '}');
    } else if (tag->type == LIST) {
        sb_append_c(sb, '[');

        bool hasMap = false;

        for (size_t i = 0; i < tag->tag_list.size; ++i) {
            if (((bso *) tag->tag_list.items[i])->type == MAP) {
                hasMap = true;
                break;
            }
        }

        if (use_newline && hasMap) sb_append_c(sb, '\n');

        for (size_t i = 0; i < tag->tag_list.size; ++i) {
            if (use_newline && hasMap) {
                for (int i = 0; i < depth + 1; ++i) {
                    sb_append_ch(sb, "  ");
                }
            }
            bso_write_as_sbso_sb(sb, (bso *)tag->tag_list.items[i], ansi_colors, spaced, use_newline, depth + 1);
            if (i + 1 < tag->tag_list.size) {
                sb_append_c(sb, ',');
                if (use_newline || spaced) {
                    sb_append_c(sb, ' ');
                    if (use_newline && hasMap) sb_append_c(sb, '\n');
                }
            }
        }

        if (use_newline && hasMap) {
            sb_append_c(sb, '\n');

            for (int i = 0; i < depth; ++i) {
                sb_append_ch(sb, "  ");
            }
        }
        sb_append_c(sb, ']');
    } else if (tag->type == BYTE_ARRAY) {
        sb_append_ch(sb, "[B;");
        for (size_t i = 0; i < tag->tag_byte_array.size; ++i) {
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
            sb_append_i(sb, tag->tag_byte_array.value[i]);
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RED);
            sb_append_c(sb, 'b');
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
            if (i + 1 < tag->tag_byte_array.size) {
                sb_append_c(sb, ',');
            }
        }
        sb_append_c(sb, ']');
    } else if (tag->type == SHORT_ARRAY) {
        sb_append_ch(sb, "[S;");
        for (size_t i = 0; i < tag->tag_short_array.size; ++i) {
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
            sb_append_i(sb, tag->tag_short_array.value[i]);
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RED);
            sb_append_c(sb, 's');
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
            if (i + 1 < tag->tag_short_array.size) {
                sb_append_c(sb, ',');
            }
        }
        sb_append_c(sb, ']');
    } else if (tag->type == INT_ARRAY) {
        sb_append_ch(sb, "[I;");
        for (size_t i = 0; i < tag->tag_int_array.size; ++i) {
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
            sb_append_i(sb, tag->tag_int_array.value[i]);
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RED);
            sb_append_c(sb, 'i');
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
            if (i + 1 < tag->tag_int_array.size) {
                sb_append_c(sb, ',');
            }
        }
        sb_append_c(sb, ']');
    } else if (tag->type == LONG) {
        sb_append_ch(sb, "[L;");
        for (size_t i = 0; i < tag->tag_long_array.size; ++i) {
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
            sb_append_i(sb, tag->tag_long_array.value[i]);
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RED);
            sb_append_c(sb, 'L');
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
            if (i + 1 < tag->tag_long_array.size) {
                sb_append_c(sb, ',');
            }
        }
        sb_append_c(sb, ']');
    } else if (tag->type == FLOAT_ARRAY) {
        sb_append_ch(sb, "[F;");
        for (size_t i = 0; i < tag->tag_float_array.size; ++i) {
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
            sb_append_i(sb, tag->tag_float_array.value[i]);
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RED);
            sb_append_c(sb, 'f');
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
            if (i + 1 < tag->tag_float_array.size) {
                sb_append_c(sb, ',');
            }
        }
        sb_append_c(sb, ']');
    } else if (tag->type == DOUBLE_ARRAY) {
        sb_append_ch(sb, "[D;");
        for (size_t i = 0; i < tag->tag_double_array.size; ++i) {
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_YELLOW);
            sb_append_i(sb, tag->tag_double_array.value[i]);
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RED);
            sb_append_c(sb, 'd');
            if (ansi_colors) sb_append_ch(sb, ANSI_COLOR_RESET);
            if (i + 1 < tag->tag_double_array.size) {
                sb_append_c(sb, ',');
            }
        }
        sb_append_c(sb, ']');
    }
}

char *bso_write_as_sbso(bso *tag, int format) {
    if (tag == NULL) return NULL;
    string_builder *sb = sb_new();
    bso_write_as_sbso_sb(sb, tag, (format & BSO_SBSO_ANSI), format & BSO_SBSO_SPACED, format & BSO_SBSO_NEWLINE, 0);
    char *output = sb_copy_data(sb);
    sb_free(sb);
    return output;
}

#endif