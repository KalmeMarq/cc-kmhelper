#ifndef KM_VECTOR_H_INCLUDED
#define KM_VECTOR_H_INCLUDED

#include <stdlib.h>
#include <stdint.h>

typedef struct dynvec {
    void **data;
    size_t size;
    size_t capacity;
} dynvec;

dynvec *dynvec_new() {
    dynvec *vec = (dynvec *)malloc(sizeof(dynvec));
    vec->capacity = 16;
    vec->data = (void **)malloc(sizeof(void *) * vec->capacity);
    vec->size = 0;
    return vec;
}

void dynvec_free(dynvec *vec) {
    for (size_t i = 0; i < vec->size; ++i) {
        free(vec->data[i]);
    }

    free(vec);
}

void dynvec_append(dynvec *vec, void *value) {
    if (vec == NULL) return;

    if (vec->size == vec->capacity) {
        void **newptr = (void**)realloc(vec->data, vec->capacity * 2);
        vec->capacity *= 2;
        vec->data = newptr;
    }

    vec->data[vec->size++] = value;
}

void dynvec_prepend(dynvec *vec, void *value) {
    if (vec == NULL) return;

    if (vec->size == vec->capacity) {
        void **newptr = (void**)realloc(vec->data, vec->capacity * 2);
        vec->capacity *= 2;
        vec->data = newptr;
    }

    for (int i = vec->size - 1; i >= 0; --i) {
        vec->data[i + 1] = vec->data[i];
    }

    vec->data[0] = value;
    vec->size++;
}

void *dynvec_get(dynvec *vec, size_t index) {
    if (vec == NULL || vec->size == 0 || index < 0 || index >= vec->size) return NULL;
    return vec->data[index];
}

void *dynvec_first(dynvec *vec) {
    if (vec == NULL || vec->size == 0) return NULL;
    return vec->data[0];
}

void *dynvec_last(dynvec *vec) {
    if (vec == NULL || vec->size == 0) return NULL;
    return vec->data[vec->size - 1];
}

void bso_pop_first(dynvec *vec) {
    if (vec == NULL || vec->size == 0) return;

    free(vec->data[0]);
    --vec->size;

    for (size_t i = 1; i <= vec->size; ++i) {
        vec->data[i - 1] = vec->data[i];
    }
}

void bso_pop_last(dynvec *vec) {
    if (vec == NULL || vec->size == 0) return;
    free(vec->data[vec->size - 1]);
    --vec->size;
}

void dynvec_clear(dynvec *vec) {
    if (vec == NULL || vec->size == 0) return;

    for (int i = 0; i < vec->size; ++i) {
        free(vec->data[i]);
    }

    vec->size = 0;
}

void dynvec_remove(dynvec *vec, size_t index) {
    if (vec == NULL || vec->size == 0 || index < 0 || index >= vec->size) return;

    free(vec->data[index]);

    for (int i = index + 1; i < vec->size; ++i) {
        vec->data[i - 1] = vec->data[i];
    }
    
    --vec->size;
}

void dynvec_list_set(dynvec *vec, void *value, size_t index) {
    if (vec == NULL || vec->size == 0 || index < 0 || index >= vec->size) return;

    free(vec->data[index]);
    vec->data[index] = value;
}

void dynvec_insert(dynvec *vec, void *value, size_t index) {
    if (vec == NULL || vec->size == 0 || index < 0 || index > vec->size) return;

    if (vec->size == vec->capacity) {
        void **newptr = (void**)realloc(vec->data, vec->capacity * 2);
        vec->capacity *= 2;
        vec->data = newptr;
    }

    for (size_t i = index; i < vec->size; ++i) {
        vec->data[i + 1] = vec->data[i];
    }

    vec->data[index] = value;
    ++vec->size;
}

// TODO: span, queue, deque, list, stack

#endif // KM_VECTOR_H_INCLUDED