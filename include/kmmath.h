#ifndef KM_MATH_H_INCLUDED
#define KM_MATH_H_INCLUDED

typedef struct {
  int x;
  int y;
} vector2i;

typedef struct {
  float x;
  float y;
} vector2f;

typedef struct {
  double x;
  double y;
} vector2d;

typedef struct {
  int x;
  int y;
  int z;
} vector3i;

typedef struct {
  float x;
  float y;
  float z;
} vector3f;

typedef struct {
  double x;
  double y;
  double z;
} vector3d;

#endif // KM_MATH_H_INCLUDED