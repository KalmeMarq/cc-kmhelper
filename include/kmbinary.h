#ifndef KM_BINARY_H_INCLUDED
#define KM_BINARY_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct binary_writer {
    uint8_t *data;
    size_t cursor;
    size_t size;
    size_t capacity;
} binary_writer;

typedef struct binary_reader {
    uint8_t *data;
    size_t cursor;
    size_t size;
} binary_reader;

/**
 * Create a new binary writer with a given initial capacity
*/
binary_writer *binary_writer_new(size_t initial_capacity) {
    binary_writer *writer = (binary_writer *)malloc(sizeof(binary_writer));
    writer->capacity = initial_capacity;
    writer->size = 0;
    writer->cursor = 0;
    writer->data = (uint8_t *)malloc(initial_capacity);
    return writer;
}

void binary_writer_ensure_capacity(binary_writer *writer, size_t size_needed) {
    if (writer->size + size_needed > writer->capacity) {
        uint8_t *new_data = (uint8_t *)realloc(writer->data, writer->capacity * 2);
        writer->capacity *= 2;

        if (new_data) {
            writer->data = new_data;
        }
    }
}

/**
 * Writes a byte (8 bits) at the current cursor position in the writer
*/
void binary_writer_put_int8(binary_writer *writer, int8_t value) {
    binary_writer_ensure_capacity(writer, 1);
    writer->data[writer->cursor++] = value;
    ++writer->size;
}

/**
 * Writes a short (2 bytes ; 16 bits) at the current cursor position in the writer
*/
void binary_writer_put_int16(binary_writer *writer, int16_t value) {
    binary_writer_ensure_capacity(writer, 2);
    uint8_t *bytes = (uint8_t *)&value;
    writer->data[writer->cursor] = bytes[1];
    writer->data[writer->cursor + 1] = bytes[0];
    writer->size += 2;
    writer->cursor += 2;
}

/**
 * Writes an int (4 bytes ; 32 bits) at the current cursor position in the writer
*/
void binary_writer_put_int32(binary_writer *writer, int32_t value) {
    binary_writer_ensure_capacity(writer, 4);
    uint8_t *bytes = (uint8_t *)&value;
    writer->data[writer->cursor] = bytes[3];
    writer->data[writer->cursor + 1] = bytes[2];
    writer->data[writer->cursor + 2] = bytes[1];
    writer->data[writer->cursor + 3] = bytes[0];
    writer->size += 4;
    writer->cursor += 4;
}

/**
 * Writes an long (8 bytes ; 64 bits) at the current cursor position in the writer
*/
void binary_writer_put_int64(binary_writer *writer, int64_t value) {
    binary_writer_ensure_capacity(writer, 8);
    uint8_t *bytes = (uint8_t *)&value;
    writer->data[writer->cursor] = bytes[7];
    writer->data[writer->cursor + 1] = bytes[6];
    writer->data[writer->cursor + 2] = bytes[5];
    writer->data[writer->cursor + 3] = bytes[4];
    writer->data[writer->cursor + 4] = bytes[3];
    writer->data[writer->cursor + 5] = bytes[2];
    writer->data[writer->cursor + 6] = bytes[1];
    writer->data[writer->cursor + 7] = bytes[0];
    writer->size += 8;
    writer->cursor += 8;
}

/**
 * Writes a float (4 bytes ; 32 bits) at the current cursor position in the writer
*/
void binary_writer_put_float32(binary_writer *writer, float value) {
    binary_writer_ensure_capacity(writer, 4);
    uint8_t *bytes = (uint8_t *)&value;
    writer->data[writer->cursor] = bytes[3];
    writer->data[writer->cursor + 1] = bytes[2];
    writer->data[writer->cursor + 2] = bytes[1];
    writer->data[writer->cursor + 3] = bytes[0];
    writer->size += 4;
    writer->cursor += 4;
}

/**
 * Writes a double (8 bytes ; 64 bits) at the current cursor position in the writer
*/
void binary_writer_put_float64(binary_writer *writer, double value) {
    binary_writer_ensure_capacity(writer, 8);
    uint8_t *bytes = (uint8_t *)&value;
    writer->data[writer->cursor] = bytes[7];
    writer->data[writer->cursor + 1] = bytes[6];
    writer->data[writer->cursor + 2] = bytes[5];
    writer->data[writer->cursor + 3] = bytes[4];
    writer->data[writer->cursor + 4] = bytes[3];
    writer->data[writer->cursor + 5] = bytes[2];
    writer->data[writer->cursor + 6] = bytes[1];
    writer->data[writer->cursor + 7] = bytes[0];
    writer->size += 8;
    writer->cursor += 8;
}

/**
 * Writes a cstring (null terminated \0) at the current cursor in the writer
*/
void binary_writer_put_cstring(binary_writer *writer, char *value) {
    size_t length = strlen(value);
    binary_writer_ensure_capacity(writer, length);

    for (size_t i = 0; i < length; ++i) {
        binary_writer_put_int8(writer, value[i]);
    }

    binary_writer_put_int8(writer, '\0');
}

/**
 * Writes a string (uint16 length prefixed) at the current cursor in the writer
*/
void binary_writer_put_string(binary_writer *writer, uint8_t *bytes, size_t length) {
    binary_writer_put_int16(writer, length);
    for (size_t i = 0; i < length; ++i) {
        binary_writer_put_int8(writer, bytes[i]);
    }
}


/**
 * Writes a string (uint16 length prefixed) from a cstring (null terminated \0) at the current cursor in the writer
*/
void binary_writer_put_string_from_cstring(binary_writer *writer, char *value) {
    size_t length = strlen(value);
    binary_writer_put_int16(writer, length);
    for (size_t i = 0; i < length; ++i) {
        binary_writer_put_int8(writer, value[i]);
    }
}

/**
 * Frees memory used by binary writer.
*/
void binary_writer_free(binary_writer *writer) {
    free(writer->data);
    free(writer);
}

/**
 * Creates a binary reader from file. It will read the whole file while saving and restoring the position that the file stream was.
*/
binary_reader *binary_reader_from_file(FILE *f) {
    binary_reader *reader = (binary_reader *)malloc(sizeof(binary_reader));
    
    size_t oldOff = ftell(f);
    
    fseek(f, 0, SEEK_END);
    size_t size = ftell(f);
    fseek(f, 0, SEEK_SET);

    reader->size = size;
    reader->cursor = 0;
    reader->data = (uint8_t *)malloc(size);

    fread(reader->data, sizeof(uint8_t), size, f);

    fseek(f, oldOff, SEEK_SET);

    return reader;
}

/**
 * Reads a byte (8 bits) at the current cursor position in the reader
*/
int8_t binary_reader_get_int8(binary_reader *reader) {
    return (int8_t)reader->data[reader->cursor++];
}

/**
 * Reads a short (2 bytes ; 16 bits) at the current cursor position in the reader
*/
int16_t binary_reader_get_int16(binary_reader *reader) {
    uint8_t bytes[2];
    bytes[1] = binary_reader_get_int8(reader);
    bytes[0] = binary_reader_get_int8(reader);
    return *(int16_t *)bytes;
}

/**
 * Reads a int (4 bytes ; 32 bits) at the current cursor position in the reader
*/
int32_t binary_reader_get_int32(binary_reader *reader) {
    uint8_t bytes[4];
    bytes[3] = binary_reader_get_int8(reader);
    bytes[2] = binary_reader_get_int8(reader);
    bytes[1] = binary_reader_get_int8(reader);
    bytes[0] = binary_reader_get_int8(reader);
    return *(int32_t *)bytes;
}

/**
 * Reads a long (8 bytes ; 64 bits) at the current cursor position in the reader
*/
int64_t binary_reader_get_int64(binary_reader *reader) {
    uint8_t bytes[8];
    bytes[7] = binary_reader_get_int8(reader);
    bytes[6] = binary_reader_get_int8(reader);
    bytes[5] = binary_reader_get_int8(reader);
    bytes[4] = binary_reader_get_int8(reader);
    bytes[3] = binary_reader_get_int8(reader);
    bytes[2] = binary_reader_get_int8(reader);
    bytes[1] = binary_reader_get_int8(reader);
    bytes[0] = binary_reader_get_int8(reader);
    return *(int64_t *)bytes;
}

/**
 * Reads a float (4 bytes ; 32 bits) at the current cursor position in the reader
*/
float binary_reader_get_float32(binary_reader *reader) {
    uint8_t bytes[4];
    bytes[3] = binary_reader_get_int8(reader);
    bytes[2] = binary_reader_get_int8(reader);
    bytes[1] = binary_reader_get_int8(reader);
    bytes[0] = binary_reader_get_int8(reader);
    return *(float *)bytes;
}

/**
 * Reads a double (8 bytes ; 64 bits) at the current cursor position in the reader
*/
double binary_reader_get_float64(binary_reader *reader) {
    uint8_t bytes[8];
    bytes[7] = binary_reader_get_int8(reader);
    bytes[6] = binary_reader_get_int8(reader);
    bytes[5] = binary_reader_get_int8(reader);
    bytes[4] = binary_reader_get_int8(reader);
    bytes[3] = binary_reader_get_int8(reader);
    bytes[2] = binary_reader_get_int8(reader);
    bytes[1] = binary_reader_get_int8(reader);
    bytes[0] = binary_reader_get_int8(reader);
    return *(double *)bytes;
}

/**
 * Reads a cstring (null terminated \0) at the current cursor position in the reader. Remember to free the memory used by the cstring
*/
char *binary_reader_get_cstring(binary_reader *reader) {
    char *data = (char *)malloc(64);

    int i = 0;
    while (i < 999999) {
        uint8_t b = reader->data[reader->cursor++];

        data[i] = b;

        if (b == '\0') {
            break;
        }
        
        ++i;
    }

    return data;
}

/**
 * Reads a string (uint16 length prefixed) at the current cursor position in the reader. Remember to free the memory used by the string
*/
char *binary_reader_get_string(binary_reader *reader) {
    uint16_t length = (uint16_t) binary_reader_get_int16(reader);
    char *data = (char *)malloc(sizeof(char) * length + 1);

    for (size_t i = 0; i < length; ++i) {
        data[i] = binary_reader_get_int8(reader);
    }

    data[length] = '\0';

    return data;
}

/**
 * Frees memory used by binary reader.
*/
void binary_reader_free(binary_reader *reader) {
    free(reader->data);
    free(reader);
}

#ifdef __cplusplus
}
#endif

#endif // KM_BINARY_H_INCLUDED