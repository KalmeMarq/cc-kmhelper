#ifndef _KM_HELPER_H_
#define _KM_HELPER_H_

// Helpers enabled by default
#define KM_INCLUDE_PRINTF_EXT
#define KM_INCLUDE_STRLEN_EXT
// #define KM_INCLUDE_RANDOM
// #define KM_INCLUDE_OPTION
// #define KM_INCLUDE_VECTOR
// #define KM_INCLUDE_STRING_VIEW
// #define KM_INCLUDE_STRING_BUILDER
// #define KM_INCLUDE_PROPERTIES_FORMAT_PARSER
// #define KM_INCLUDE_JSON_FORMAT_PARSER
// #define KM_INCLUDE_BINARY_WRITER_READER

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

//{
  
#define and &&
#define or ||
#define xor ^
#define not !
#define bitand &
#define bitor |

typedef struct result {
  void *rvalue;
  char *error;
  bool has_error;
} result;

#define resultOk(value) { value, NULL, false }
#define resultErr(errormsg) { NULL, errormsg, true }

//}

#ifdef KM_INCLUDE_RANDOM
#include <time.h>
#endif

#define KM_SAFE_ITER_INDEX_REACH 65536

#ifdef KM_INCLUDE_RANDOM
//{

/**
 * Sets the global pseudo-random generator seed as the current time
*/
void random_set_time_based_seed() {
  srand(time(NULL));
}

/**
 * Sets the global pseudo-random generator seed
*/
void random_set_seed(int seed) {
  srand(seed);
}

/**
 * Returns a pseudo-random integer value
*/
int random_get_int() {
  return rand();
}

/**
 * Returns a pseudo-random integer value in the range specified
*/
int random_get_int_range(int min, int max) {
  return min + rand() % (max + 1 - min);
}

/**
 * Returns a pseudo-random float value in the range specified
*/
float random_get_float_range(float min, float max) {
  return ((min * 10000) + rand() * 10000 % (int) (max* 10000 + 1 - min * 10000)) / 10000.0f;
}

/**
 * Returns a pseudo-random float value between 0.0 and 1.0
*/
float random_get_float() {
  return random_get_float_range(0.0f, 1.0f);
}

//}
#endif

#ifdef KM_INCLUDE_PRINTF_EXT
//{ printf extensions

/**
 * It does the same as printf but adds a new line to the end of the message
*/
#define printfln(format, ...) printf(format "\n", __VA_ARGS__)

/**
 * Prints message to the stdout with a new line at the end
*/
#define println(format) printf(format "\n")

//}
#endif

#ifdef KM_INCLUDE_STRLEN_EXT
//{ util functions to calculate accurate lengths

/**
 * Calculates the length of the given cstring. It does not add to the count the null terminator (\0) and it also handles utf8.
 */
int strlength(const char *str) {
  // Checks if it's a null pointer
  if (str == NULL) {
    return 0;
  }

  int len = 0; // Amount of actual characters
  
  int i = 0;
  while (i < KM_SAFE_ITER_INDEX_REACH) {
    size_t bt = str[i];

    if (bt == '\0') { // If byte is null terminator that's the end of the cstring
      break;
    }

    if ((bt & 0x80) == 0) { // If the leftmost bit is 0 then it's a 1 byte character
      ++len;
    } else if ((bt & 0xE0) == 0xC0) { // If the 3 leftmost bits are 110 then it's a 2 byte character
      ++len;
      ++i;
    } else if ((bt & 0xF0) == 0xE0) { // If the 4 leftmost bits are 1110 then it's a 3 byte character
      ++len;
      i += 2;
    } else if ((bt & 0xF8) == 0xF0) { // If the 5 leftmost bits are 11110 then it's a 4 byte character
      ++len;
      i += 3;
    } else if ((bt & 0xFC) == 0xF8) {
      ++len;
      i += 4;
    } else if ((bt & 0xFE) == 0xFC) {
      ++len;
      i += 5;
    }

    ++i;
  }

  return len;
}

/**
 * Calculates the length of the given wide cstring. It does not add to the count the null terminator (\0) and it also handles utf8.
 */
int wstrlength(const wchar_t *wstr) {
  // Checks if it's a null pointer
  if (wstr == NULL) {
    return 0;
  }

  int len = 0; // Amount of actual characters

  int i = 0;
  while (i < KM_SAFE_ITER_INDEX_REACH) {
    size_t bt = wstr[i];

    if (bt == '\0') { // If byte is null terminator that's the end of the cstring
      break;
    }

    if (wstr[i + 1] != '\0') {
      size_t b = bt << 8 | wstr[i + 1];

      if ((b & 0xFC00) < 0xD800 || (b & 0xFC00) > 0xDFFF) {
        ++i;
      }
      ++len;
    } else {
      ++len;
    }
    
    ++i;
  }

  return len;
}

/**
 * Calculates the content length in the given cstring by ignoring whitespace. It does not add to the count the null terminator (\0) and it also handles utf8.
 */
int strcntlength(const char *str) {
  // Checks if it's a null pointer
  if (str == NULL) {
    return 0;
  }

  int len = 0; // Amount of actual characters
  
  int i = 0;
  while (i < KM_SAFE_ITER_INDEX_REACH) {
    int bt = str[i];

    if (bt == '\0') { // If byte is null terminator that's the end of the cstring
      break;
    }

    if ((bt & 0x80) == 0) { // If the leftmost bit is 0 then it's a 1 byte character
      if (bt != ' ' && bt != '\f' && bt != '\r' && bt != '\n' && bt != '\t') { // Prevent whitespace characters from adding to the length
        ++len;
      }
    } else if ((bt & 0xE0) == 0xC0) { // If the 3 leftmost bits are 110 then it's a 2 byte character
      ++len;
      ++i;
    } else if ((bt & 0xF0) == 0xE0) { // If the 4 leftmost bits are 1110 then it's a 3 byte character
      ++len;
      i += 2;
    } else if ((bt & 0xF8) == 0xF0) { // If the 5 leftmost bits are 11110 then it's a 4 byte character
      ++len;
      i += 3;
    } else if ((bt & 0xFC) == 0xF8) {
      ++len;
      i += 4;
    } else if ((bt & 0xFE) == 0xFC) {
      ++len;
      i += 5;
    }

    ++i;
  }

  return len;
}

/**
 * Calculates the content length in the given wide cstring by ignoring whitespace. It does not add to the count the null terminator (\0) and it also handles utf8.
 */
int wstrcntlength(const wchar_t *wstr) {
  // Checks if it's a null pointer
  if (wstr == NULL) {
    return 0;
  }

  int len = 0; // Amount of actual characters

  int i = 0;
  while (i < KM_SAFE_ITER_INDEX_REACH) {
    size_t bt = wstr[i];

    if (bt == '\0') { // If byte is null terminator that's the end of the cstring
      break;
    }

    if (bt == ' ' || bt == '\r' || bt == '\t' || bt == '\f') {
      ++i;
    } else if (wstr[i + 1] != '\0') {
      size_t b = bt << 8 | wstr[i + 1];

      if ((b & 0xFC00) < 0xD800 || (b & 0xFC00) > 0xDFFF) {
        ++i;
      }
      ++len;
    } else {
      ++len;
    }
    
    ++i;
  }

  return len;
}

//}
#endif

#ifdef KM_INCLUDE_OPTION
//{ Rust like Option

#define none() { .has_value = false }
#define some(vl) { .has_value = true, .value = vl }

#define option_new_type(type) typedef struct { bool has_value; type value; } option_ ## type; \

option_new_type(bool)

option_new_type(char)
option_new_type(int)
option_new_type(long)
option_new_type(float)
option_new_type(double)

option_new_type(int8_t)
option_new_type(int16_t)
option_new_type(int32_t)
option_new_type(int64_t)
option_new_type(uint8_t)
option_new_type(uint16_t)
option_new_type(uint32_t)
option_new_type(uint64_t)

//}
#endif

#ifdef KM_INCLUDE_VECTOR
//{ Vector

typedef struct vec {
  void **_data;
  size_t _element_size;
  size_t _capacity;
  size_t _size;
} vec;

/**
 * Creates a new vector with elements of size element_size. Do not forget to free the vector (use vec_free)
*/
vec *vec_new(size_t _element_size) {
  vec *self = (vec *)malloc(sizeof(vec));
  self->_capacity = 16;
  self->_size = 0;
  self->_element_size = _element_size;
  self->_data = (void **)malloc(_element_size * self->_capacity);
  return self;
}

static void vec_resize(vec *self, int _capacity) {
  void **_data = (void **)realloc(self->_data, self->_element_size * _capacity);

  if (_data) {
    self->_data = _data;
    self->_capacity = _capacity;
  }
}

/**
 * Add an item to the end of the vector
*/
void vec_add(vec *self, void *item) {
  if (self->_capacity == self->_size) {
    vec_resize(self, self->_capacity * 2);
  }
  self->_data[self->_size++] = item;
}

/**
 * Set item in vector at the given index. If the index is out of bounds nothing will happen
*/
void vec_set(vec *self, int index, void *item) {
  if (index >= 0 && index < self->_size) {
    self->_data[index] = item;
  }
}

/**
 * Get the item at the given index in the vector. If the index is out of bounds it will return NULL 
*/
void *vec_get(vec *self, int index) {
  if (index >= 0 && index < self->_size) {
    return self->_data[index];
  }

  return NULL;
}

/**
 * Get the _size of the vector
*/
int vec_size(vec *self) {
  return self->_size;
}

/**
 * Free the memory used by the vector
*/
void vec_free(vec *self) {
  free(self->_data);
  free(self);
}

//}
#endif

#ifdef KM_INCLUDE_STRING_VIEW
//{ string_view - Implementation is based on C++ std::string_view but with extra features)

typedef struct string_view {
  const char *data;
  size_t size;
} string_view;

string_view sv_new(const char *str);

/**
 * Gets the char at the specified index. If index is out of bounds a null char will be returned
*/
char sv_at(string_view sv, size_t index);

/**
 * Gets the first character. If the view is empty a null char will be returned
*/
char sv_front(string_view sv);

/**
 * Gets the last character. If the view is empty a null char will be returned
*/
char sv_back(string_view sv);

/**
 * Checks whether the view is empty
*/
bool sv_empty(string_view sv);

/**
 * Checks if the view starts with the given prefix
*/
bool sv_starts_with(string_view sv, string_view prefix);

/**
 * Checks if the view ends with the given prefix
*/
bool sv_ends_with(string_view sv, string_view prefix);

/**
 * Removes whitespace from both ends of the view and returns the result
*/
string_view sv_trim(string_view sv);

/**
 * Removes whitespace from the beginning of the view and returns the result
*/
string_view sv_trim_left(string_view sv);

/**
 * Removes whitespace from the end of the view and returns the result
*/
string_view sv_trim_right(string_view sv);

/**
 * Shrinks the view by moving its start forward and returns what was chopped
*/
string_view sv_chop_left(string_view *sv, size_t n);

/**
 * Shrinks the view by moving its end backward and returns what was chopped
*/
string_view sv_chop_right(string_view *sv, size_t n);

/**
 * Shrinks the view by moving its start forward
*/
void sv_remove_prefix(string_view *sv, size_t n);

/**
 * Shrinks the view by moving its end backward
*/
void sv_remove_suffix(string_view *sv, size_t n);

/**
 * Swaps the contents of the views
*/
void sv_swap(string_view *sv0, string_view *sv1);

/**
 * Returns the first index at which the given char can be found or -1 if it is not present 
*/
int sv_index_of(string_view sv, char c);

/**
 * Returns the last index at which the given char can be found or -1 if it is not present
*/
int sv_last_index_of(string_view sv, char c);

/**
 * Returns the last index at which the given substring can be found or -1 if it is not present
*/
int sv_last_index_of_sv(string_view sv, string_view c);

/**
 * Returns a view of the substring start...start + count
*/
string_view sv_substr(string_view sv, size_t start, size_t count);

/**
 * Checks if the view contains the given character
*/
bool sv_contains(string_view sv, char c);

/**
 * Checks if the view contains the substring
*/
bool sv_contains_sv(string_view sv, string_view c);

/**
 * Compares two views
*/
bool sv_compare(string_view sv0, string_view sv1);

/**
 * Returns the first index at which the given char can be found starting at the given position or -1 if it is not present 
*/
int sv_find(string_view sv, char c, size_t pos);

/**
 * Returns the first index at which the given substring can be found starting at the given position or -1 if it is not present 
*/
int sv_find_sv(string_view sv, string_view c, size_t pos);

/**
 * Copies the content of the view to a char array
*/
void sv_copy(char *dest, string_view sv);

/**
 * Copies the content of the view to a char array
*/
void sv_copy_s(char *dest, string_view sv, size_t size);

#define sv(string) sv_new(string)

static string_view sv_construct(const char *data, size_t size) {
  string_view sv;
  sv.data = data;
  sv.size = size;
  return sv;
} 

string_view sv_new(const char *str) {
  return sv_construct(str, strlen(str));
}

char sv_at(string_view sv, size_t index) {
  if (index < 0 || index >= sv.size) {
    return '\0';
  }

  return sv.data[index];
}

char sv_front(string_view sv) {
  if (sv.size == 0) {
    return '\0';
  }

  return sv.data[0];
}

char sv_back(string_view sv) {
  if (sv.size == 0) {
    return '\0';
  }

  return sv.data[sv.size - 1];
}

bool sv_empty(string_view sv) {
  return sv.size == 0;
}

bool sv_starts_with(string_view sv, string_view prefix) {
  if (prefix.size > sv.size) return false;
  return sv_compare(sv_construct(sv.data, prefix.size), prefix);
}

bool sv_ends_with(string_view sv, string_view suffix) {
  if (suffix.size > sv.size) return false;
  return sv_compare(sv_construct(sv.data + sv.size - suffix.size, suffix.size), suffix);
}

string_view sv_trim_left(string_view sv) {
  int i = 0;
  while (i < sv.size && (sv.data[i] == ' ' || sv.data[i] == '\f' || sv.data[i] == '\n' || sv.data[i] == '\r' || sv.data[i] == '\v')) {
    i += 1;
  }
  return sv_construct(sv.data + i, sv.size - i);
}

string_view sv_trim_right(string_view sv) {
  int i = 0;
  while (i < sv.size && (sv.data[sv.size - 1 - i] == ' ' || sv.data[sv.size - 1 - i] == '\f' || sv.data[sv.size - 1 - i] == '\n' || sv.data[sv.size - 1 - i] == '\r' || sv.data[sv.size - 1 - i] == '\v')) {
    i += 1;
  }
  return sv_construct(sv.data, sv.size - i);
}

string_view sv_chop_left(string_view *sv, size_t n) {
  if (n > sv->size) {
    sv->data += sv->size;
    sv->size = 0;
    return sv_construct(sv->data, 0);
  }

  string_view res = sv_construct(sv->data, n);
  sv->data += n;
  sv->size -= n;
  return res;
}

string_view sv_chop_right(string_view *sv, size_t n) {
  if (n > sv->size) {
    sv->size = 0;
    return sv_construct(sv->data, 0);
  }

  string_view res = sv_construct(sv->data + sv->size - n, n);
  sv->size -= n;
  return res;
}

void sv_remove_prefix(string_view *sv, size_t n) {
 if (n > sv->size) {
    sv->data += sv->size;
    sv->size = 0;
  } else {
    sv->data += n;
    sv->size -= n;
  }
}

void sv_remove_suffix(string_view *sv, size_t n) {
 if (n > sv->size) {
    sv->size = 0;
  } else {
    sv->size -= n;
  }
}

void sv_swap(string_view *sv0, string_view *sv1) {
  const char *sv0data = sv0->data;
  const char *sv1data = sv1->data;
  size_t sv0size = sv0->size;
  size_t sv1size = sv1->size;

  sv0->data = sv1data;
  sv0->size = sv1size;
  sv1->data = sv0data;
  sv1->size = sv0size;
}

string_view sv_trim(string_view sv) {
  return sv_trim_left(sv_trim_right(sv));
}

bool sv_compare(string_view sv0, string_view sv1) {
  if (sv0.size != sv1.size) return false;
  else {
    return memcmp(sv0.data, sv1.data, sv0.size) == 0;
  }
}

int sv_find(string_view sv, char c, size_t pos) {
  int i = pos;

  while (i < sv.size && sv.data[i] != c) {
    i += 1;
  }
  
  return i < sv.size ? i : -1;
}

int sv_find_sv(string_view sv, string_view c, size_t pos) {
  if (c.size > sv.size) {
    return -1;
  }

  int containsIdx = -1;
  size_t i = pos;
  size_t j = 0;
  while (i < sv.size) {
    bool idxF = true;
    
    while (j < c.size) {
      if (j + i >= sv.size) {
        idxF = false;
        break;
      }

      if (sv.data[i + j] != c.data[j]) {
        idxF = false;
        break;
      }
      ++j;
    }

    if (idxF) {
      containsIdx = i;
      break;
    }

    ++i;
  }

  return containsIdx;
}

int sv_index_of(string_view sv, char c) {
  int i = 0;

  while (i < sv.size && sv.data[i] != c) {
    i += 1;
  }
  
  return i < sv.size ? i : -1;
}

int sv_index_of_sv(string_view sv, string_view c) {
  if (c.size > sv.size) {
    return -1;
  }

  int containsIdx = -1;
  int i = 0;
  int j = 0;
  while (i < sv.size) {
    bool idxF = true;
    
    while (j < c.size) {
      if (j + i >= sv.size) {
        idxF = false;
        break;
      }

      if (sv.data[i + j] != c.data[j]) {
        idxF = false;
        break;
      }
      ++j;
    }

    if (idxF) {
      containsIdx = i;
      break;
    }

    ++i;
  }

  return containsIdx;
}

int sv_last_index_of(string_view sv, char c) {
  int i = 0;

  while (i < sv.size && sv.data[sv.size - i - 1] != c) {
    i += 1;
  }
  
  return i < sv.size ? sv.size - 1 - i : -1;
}

int sv_last_index_of_sv(string_view sv, string_view c) {
  if (c.size > sv.size) {
    return -1;
  }

  int containsIdx = -1;
  int i = sv.size - 1;
  int j = 0;
  while (i >= 0) {
    bool idxF = true;
    
    while (j < c.size) {
      if (j + i >= sv.size) {
        idxF = false;
        break;
      }

      if (sv.data[i + j] != c.data[j]) {
        idxF = false;
        break;
      }
      ++j;
    }

    if (idxF) {
      containsIdx = i;
      break;
    }

    --i;
  }

  return containsIdx;
}

string_view sv_substr(string_view sv, size_t start, size_t count) {
  string_view res = sv_construct(sv.data, sv.size);
  res.data += start;
  res.size = count > sv.size ? sv.size : count;
  return res;
}

bool sv_contains(string_view sv, char c) {
  return sv_index_of(sv, c) >= 0;
}

bool sv_contains_sv(string_view sv, string_view c) {
  return sv_index_of_sv(sv, c) >= 0;
}

void sv_copy(char *dest, string_view sv) {
  memcpy(dest, sv.data, sv.size);
  dest[sv.size] = '\0';
  return;
}

void sv_copy_s(char *dest, string_view sv, size_t size) {
  memcpy(dest, sv.data, size > sv.size ? sv.size : size);
  dest[sv.size] = '\0';
  return;
}

//}
#endif

#ifdef KM_INCLUDE_STRING_BUILDER
//{ String Builder from Java

typedef struct string_builder {
  size_t size;
  size_t capacity;
  char *data;
} string_builder;

string_builder *sb_new();

/**
 * Appends the given char to the sequence
*/
void sb_append_c(string_builder *sb, const char c);

/**
 * Appends the given char array to the sequence
*/
void sb_append_ch(string_builder *sb, const char *c);

/**
 * Appends the given substring to the sequence
*/
void sb_append_sv(string_builder *sb, string_view sv);

/**
 * Appends a newline to the sequence
*/
void sb_append_newline(string_builder *sb);

/**
 * Appends the given int value to the sequence
*/
void sb_append_i(string_builder *sb, int value);

/**
 * Appends the given float value to the sequence
*/
void sb_append_f(string_builder *sb, float value);

/**
 * Appends the given double value to the sequence
*/
void sb_append_d(string_builder *sb, double value);

/**
 * Reverses the sequence
*/
void sb_reverse(string_builder *sb);

/**
 * Copy the contents to the destination
*/
void sb_to_str(string_builder *sb, char *dest);

string_builder *sb_new() {
  string_builder* builder = (string_builder *)malloc(sizeof(string_builder));
  builder->capacity = 64;
  builder->size = 0;
  builder->data = (char *)malloc(sizeof(char) * builder->capacity);
  return builder;
}

static void sb_resize(string_builder *sb, size_t capacity) {
  char *data = (char *)realloc(sb->data, sizeof(char) * capacity);
  
  if (data) {
    sb->data = data;
    sb->capacity = capacity;
  }
}

void sb_free(string_builder *sb) {
  free(sb->data);
  free(sb);
}

void sb_append_c(string_builder *sb, const char c) {
  if (sb->size == sb->capacity) {
    sb_resize(sb, sb->capacity * 2);
  }

  sb->data[sb->size] = c;
  ++sb->size;
}

void sb_append_ch(string_builder *sb, const char *c) {
  int len = strlen(c);

  for (int i = 0; i < len; ++i) {
    if (c[i] != '\0') sb_append_c(sb, c[i]);
  }
}

void sb_append_sv(string_builder *sb, string_view sv) {
  int len = sv.size;

  for (int i = 0; i < len; ++i) {
    sb_append_c(sb, sv.data[i]);
  }
}

void sb_append_newline(string_builder *sb) {
  sb_append_c(sb, '\n');
}

void sb_append_i(string_builder *sb, int value) {
  char temp[32];
  snprintf(temp, 32, "%d", value);
  char *temp1 = (char *)malloc(strlen(temp) + 1);
  memcpy(temp1, temp, strlen(temp) + 1);
  sb_append_ch(sb, temp1);
  free(temp1);
}

void sb_append_f(string_builder *sb, float value) {
  int len = snprintf(NULL, 0, "%f", value);
  char *result = (char *)malloc(len + 1);
  snprintf(result, len + 1, "%f", value);
  sb_append_ch(sb, result);
  free(result);
}

void sb_append_d(string_builder *sb, double value) {
  int len = snprintf(NULL, 0, "%f", value);
  char *result = (char *)malloc(len + 1);
  snprintf(result, len + 1, "%f", value);
  sb_append_ch(sb, result);
  free(result);
}

void sb_reverse(string_builder *sb) {
  int len = sb->size;

  if (len == 0) return;

  char *temp = (char *)malloc(sb->capacity);
  strcpy(temp, sb->data);

  for (int i = 0; i < len; ++i) {
    sb->data[i] = temp[len - 1 - i];
  }

  free(temp);
}

void sb_to_str(string_builder *sb, char *dest) {
  memcpy(dest, sb->data, sb->size);
  dest[sb->size] = '\0';
}

//}
#endif

//{

#define pair_new_type(type0, type1) typedef struct { type0 first; type1 second; } pair_ ## type0 ##_ ## type1; \

//}

//{

#define m_min(a, b) (a < b ? a : b)
#define m_max(a, b) (a < b ? b : a)
#define m_clamp(v, n, m) (v < n ? n : v > m ? m : v)
#define m_abs(a) (a < 0 ? -a : a)
#define m_sign(a) (a < 0 ? -1 : 1)

float m_lerp_f(float a, float b, float t) {
  return a;
}

double m_lerp_d(double a, double b, double t) {
  return a;
}

#if __STDC_VERSION__ >= 201112L

#define m_lerp(a, b, t) _Generic((a), \
  float: m_lerp_f(a, b, t), \
  double: m_lerp_d(a, b, t) \
) \

#define M_PI 3.14159
#define M_E 2.718

typedef struct {
  int x;
  int y;
} vector2i;

typedef struct {
  float x;
  float y;
} vector2f;

typedef struct {
  double x;
  double y;
} vector2d;

typedef struct {
  int x;
  int y;
  int z;
} vector3i;

typedef struct {
  float x;
  float y;
  float z;
} vector3f;

typedef struct {
  double x;
  double y;
  double z;
} vector3d;

#endif

//}

//{

/**
 * Reads the contents of a file as string
*/
char *read_file_as_string(FILE *textfile) {
  long numbytes;

  if(textfile == NULL) {
    return NULL;
  }
    
  fseek(textfile, 0L, SEEK_END);
  numbytes = ftell(textfile);
  fseek(textfile, 0L, SEEK_SET);  

  char *text = (char*)calloc(numbytes, sizeof(char));   
  if(text == NULL) {
    return NULL;
  }

  fread(text, sizeof(char), numbytes, textfile);
  fseek(textfile, 0L, SEEK_SET);
  return text;
}

/**
 * Clears the terminal window. It's compatible with Windows, Apple and Unix-based systems
*/
void clear_screen() {
  #ifdef _WIN32
    system("cls");
  #endif

  #if defined(__linux__) || defined(__unix__) || defined(__APPLE__)
    system("clear");
  #endif
}

//}

#ifdef KM_INCLUDE_PROPERTIES_FORMAT_PARSER
//{ .properties file parser

/*
  Example:
    FILE *textfile = fopen("test0.properties", "r");
    char *text = read_file_as_string(textfile);
    fclose(textfile);

    // Use text and free it at the end

    properties_file *fp = parse_properties_file(text);

    printfln("Found %d properties", (int) fp->size);
    for (int i = 0; i < fp->size; ++i) {
        if (fp->entries[i]->has_value) {
            printfln("[key=%s ; value=%s]", fp->entries[i]->key, fp->entries[i]->value);
        } else {
            printfln("[key=%s]", fp->entries[i]->key);
        }
    }

    properties_file_free(fp);
    free(text);
*/

typedef struct properties_file_entry {
    char *key;
    char *value;
    bool has_value;
} properties_file_entry;

typedef struct properties_file {
    properties_file_entry **entries;
    size_t size;
    size_t capacity;
} properties_file;

bool is_alphanum_c(char c) {
    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9')) {
        return true;
    }
    return false;
}

bool is_string_valid(char c) {
    if (is_alphanum_c(c) || c == '/' || c == '\\' || c == ':'  || c == '.') {
        return true;
    }
    return false;
}

void skip_whitespace(char *strptr, size_t *cursor, int len) {
    while (*cursor < len && (strptr[*cursor] == ' ' || strptr[*cursor] == '\t' || strptr[*cursor] == '\r')) {
        ++(*cursor);
    }
}

void skip_whitespace_until_newline(char *strptr, size_t *cursor, int len) {
    while (*cursor < len && (strptr[*cursor] == ' ' || strptr[*cursor] == '\t' || strptr[*cursor] == '\r')) {
        ++(*cursor);
        if (strptr[*cursor] == '\n') {
            break;
        }
    }
}

void skip_until_newline(char *strptr, size_t *cursor, int len) {
  while (*cursor < len && strptr[*cursor] != '\n') {
    ++(*cursor);
  }
}

/**
 * Parses a .properties file and returns a result contains either an parsing error or the entries of the file
*/
result parse_properties_file(char *pstr) {
  properties_file *pf = (properties_file *)malloc(sizeof(properties_file));
  pf->capacity = 8;
  pf->size = 0;
  pf->entries = (properties_file_entry **)malloc(sizeof(properties_file) * pf->capacity);

  char *strptr = pstr;

  int len = strlen(pstr);
  size_t cursor = 0;

  while (cursor < len) {
    if (strptr[cursor] == '#' || strptr[cursor] == '!') {
      skip_until_newline(strptr, &cursor, len);
    }

    if (is_alphanum_c(strptr[cursor])) {
      int keylen = 0;
      int keyCursorStart = cursor;
      while (cursor < len && is_string_valid(strptr[cursor])) {
        ++keylen;
        ++cursor;
      }

      skip_whitespace(strptr, &cursor, len);

      if (strptr[cursor] == '\n' || strptr[cursor] == '\0') {
        properties_file_entry *entry = (properties_file_entry *)malloc(sizeof(properties_file_entry));
        entry->key = (char *)calloc(keylen + 1, sizeof(char));
        entry->has_value = false;

        for (size_t hj = 0; hj < keylen; ++hj) {
          entry->key[hj] = strptr[keyCursorStart + hj];
        }
        entry->key[keylen] = '\0';

        pf->entries[pf->size] = entry;

        ++pf->size;
      } else {
        if (strptr[cursor] == '=' || strptr[cursor] == ':') {
          ++cursor;
          skip_whitespace(strptr, &cursor, len);
        }
          
        int vllen = 0;
        int vlCursorStart = cursor;
        while (cursor < len && (is_string_valid(strptr[cursor]) || strptr[cursor] == ' ')) {
          ++vllen;
          ++cursor;

          if (strptr[cursor] == '\\') {
            while (cursor < len && strptr[cursor] != '\n') {
              ++cursor;
            }

            while (cursor < len && strptr[cursor] == '\n') {
              ++cursor;
            }
          }
        }

        properties_file_entry *entry = (properties_file_entry *)malloc(sizeof(properties_file_entry));
        entry->key = (char *)calloc(keylen + 1, sizeof(char));
        entry->value = (char *)calloc(vllen + 1, sizeof(char));
        entry->has_value = true;

        for (size_t hj = 0; hj < keylen; ++hj) {
          entry->key[hj] = strptr[keyCursorStart + hj];
        }

        for (size_t hj = 0; hj < vllen; ++hj) {
          entry->value[hj] = strptr[vlCursorStart + hj];
        }

        entry->key[keylen] = '\0';
        entry->value[vllen] = '\0';
        pf->entries[pf->size] = entry;

        skip_whitespace_until_newline(strptr, &cursor, len);
        ++pf->size;
      }
    }

    if (strptr[cursor] == '\n') ++cursor;
    skip_whitespace(strptr, &cursor, len);
  }

  result res = resultOk(pf);
  return res; 
}

/**
 * Frees the memory allocated by the parsed properties file
*/
void properties_file_free(properties_file *pf) {
  for (size_t i = 0; i < pf->size; ++i) {
    free(pf->entries[i]->key);
    free(pf->entries[i]->value);
    free(pf->entries[i]);
  }

  free(pf->entries);
  free(pf);
}

//}
#endif

#ifdef KM_INCLUDE_BINARY_WRITER_READER
//{

typedef struct binary_writer {
  char *buffer;
  size_t cursor;
  size_t size;
  size_t capacity;
} binary_writer;

/**
 * Creates a new binary writer with the given initial capacity. Remember to free it after using it.
*/
binary_writer *bw_new(size_t initial_capacity);

/**
 * Ensures the writer as the needed capacity
*/
void bw_ensure_capacity(binary_writer *writer, size_t needed_size);

/**
 * Writes a 8-bit integer value
*/
void bw_write_int8(binary_writer *writer, int8_t value);

/**
 * Writes a 16-bit integer value with big-endian
*/
void bw_write_int16BE(binary_writer *writer, int16_t value);

/**
 * Writes a 16-bit integer value with little-endian
*/
void bw_write_int16LE(binary_writer *writer, int16_t value);

/**
 * Writes a 32-bit integer value with big-endian
*/
void bw_write_int32BE(binary_writer *writer, int32_t value);

/**
 * Writes a 32-bit integer value with little-endian
*/
void bw_write_int32LE(binary_writer *writer, int32_t value);

/**
 * Writes a 64-bit integer value with big-endian
*/
void bw_write_int64BE(binary_writer *writer, int64_t value);

/**
 * Writes a 64-bit integer value with little-endian
*/
void bw_write_int64LE(binary_writer *writer, int64_t value);

/**
 * Writes a 32-bit float value with big-endian
*/
void bw_write_float32BE(binary_writer *writer, float value);

/**
 * Writes a 32-bit float value with little-endian
*/
void bw_write_float32LE(binary_writer *writer, float value);

binary_writer *bw_new(size_t initial_capacity) {
  binary_writer *ptr = malloc(sizeof(binary_writer));
  ptr->capacity = initial_capacity;
  ptr->size = initial_capacity;
  ptr->cursor = 0;
  ptr->buffer = (char *)malloc(initial_capacity);
  return ptr;
}

void bw_ensure_capacity(binary_writer *writer, size_t needed_size) {
  if (writer->cursor + needed_size > writer->capacity) {
    char *nptr = realloc(writer->buffer, writer->capacity * 2);

    writer->buffer = nptr;
    writer->size = writer->capacity * 2;
    writer->capacity = writer->size;
  }
}

void bw_write_int8(binary_writer *writer, int8_t value) {
  bw_ensure_capacity(writer, 1);
  writer->buffer[writer->cursor] = (char) (value & 0xFF);
  ++writer->cursor;
}

void bw_write_int16BE(binary_writer *writer, int16_t value) {
  bw_ensure_capacity(writer, 2);
  writer->buffer[writer->cursor] = (char) ((value >> 8) & 0xFF);
  writer->buffer[writer->cursor + 1] = (char) (value && 0xFF);
  writer->cursor += 2;
}

void bw_write_int16LE(binary_writer *writer, int16_t value) {
  bw_ensure_capacity(writer, 2);
  writer->buffer[writer->cursor + 1] = (char) ((value >> 8) & 0xFF);
  writer->buffer[writer->cursor] = (char) (value && 0xFF);
  writer->cursor += 2;
}

void bw_write_int32BE(binary_writer *writer, int32_t value) {
  bw_ensure_capacity(writer, 4);
  writer->buffer[writer->cursor] = (char) ((value >> 24) & 0xFF);
  writer->buffer[writer->cursor + 1] = (char) ((value >> 16) & 0xFF);
  writer->buffer[writer->cursor + 2] = (char) ((value >> 8) & 0xFF);
  writer->buffer[writer->cursor + 3] = (char) (value & 0xFF);
  writer->cursor += 4;
}

void bw_write_int32LE(binary_writer *writer, int32_t value) {
  bw_ensure_capacity(writer, 4);
  writer->buffer[writer->cursor + 3] = (char) ((value >> 24) & 0xFF);
  writer->buffer[writer->cursor + 2] = (char) ((value >> 16) & 0xFF);
  writer->buffer[writer->cursor + 1] = (char) ((value >> 8) & 0xFF);
  writer->buffer[writer->cursor] = (char) (value & 0xFF);
  writer->cursor += 4;
}

void bw_write_int64BE(binary_writer *writer, int64_t value) {
  bw_ensure_capacity(writer, 8);
  writer->buffer[writer->cursor] = (char) ((value >> 56) & 0xFF);
  writer->buffer[writer->cursor + 1] = (char) ((value >> 48) & 0xFF);
  writer->buffer[writer->cursor + 2] = (char) ((value >> 40) & 0xFF);
  writer->buffer[writer->cursor + 3] = (char) ((value >> 32) & 0xFF);
  writer->buffer[writer->cursor + 4] = (char) ((value >> 24) & 0xFF);
  writer->buffer[writer->cursor + 5] = (char) ((value >> 16) & 0xFF);
  writer->buffer[writer->cursor + 6] = (char) ((value >> 8) & 0xFF);
  writer->buffer[writer->cursor + 7] = (char) (value & 0xFF);
  writer->cursor += 8;
}

void bw_write_int64LE(binary_writer *writer, int64_t value) {
  bw_ensure_capacity(writer, 8);
  writer->buffer[writer->cursor + 7] = (char) ((value >> 56) & 0xFF);
  writer->buffer[writer->cursor + 6] = (char) ((value >> 48) & 0xFF);
  writer->buffer[writer->cursor + 5] = (char) ((value >> 40) & 0xFF);
  writer->buffer[writer->cursor + 4] = (char) ((value >> 32) & 0xFF);
  writer->buffer[writer->cursor + 3] = (char) ((value >> 24) & 0xFF);
  writer->buffer[writer->cursor + 2] = (char) ((value >> 16) & 0xFF);
  writer->buffer[writer->cursor + 1] = (char) ((value >> 8) & 0xFF);
  writer->buffer[writer->cursor] = (char) (value & 0xFF);
  writer->cursor += 8;
}

char* bw_floatToByteArray_priv(float f) {
  char* ret = malloc(4 * sizeof(char));
  unsigned int asInt = *((int*)&f);

  int i;
  for (i = 0; i < 4; i++) {
    ret[i] = (asInt >> 8 * i) & 0xFF;
  }

  return ret;
}

void bw_write_float32BE(binary_writer *writer, float value) {
  bw_ensure_capacity(writer, 4);
  char *fBA = bw_floatToByteArray_priv(value);
  writer->buffer[writer->cursor] = (char) (fBA[0] & 0xFF);
  writer->buffer[writer->cursor + 1] = (char) (fBA[1] & 0xFF);
  writer->buffer[writer->cursor + 2] = (char) (fBA[2] & 0xFF);
  writer->buffer[writer->cursor + 3] = (char) (fBA[3] & 0xFF);
  free(fBA);
  writer->cursor += 4;
}

void bw_write_float32LE(binary_writer *writer, float value) {
  bw_ensure_capacity(writer, 4);
  char *fBA = bw_floatToByteArray_priv(value);
  writer->buffer[writer->cursor + 3] = (char) (fBA[0] & 0xFF);
  writer->buffer[writer->cursor + 2] = (char) (fBA[1] & 0xFF);
  writer->buffer[writer->cursor + 1] = (char) (fBA[2] & 0xFF);
  writer->buffer[writer->cursor] = (char) (fBA[3] & 0xFF);
  free(fBA);
  writer->cursor += 4;
}

void bw_free(binary_writer *ptr) {
  free(ptr->buffer);
  free(ptr);
}

void bw_write_to_file(binary_writer *ptr, FILE *f) {
  fwrite(ptr->buffer, ptr->cursor, 1, f);
}

typedef struct binary_reader {
  char *buffer;
  size_t size;
  size_t cursor;
} binary_reader;

/**
 * Create a new binary reader from a file. Remember to free it after using it.
*/
binary_reader *br_new_from_file(FILE *f);

/**
 * Reads an 8-bit integer value
*/
int8_t br_read_int8(binary_reader *reader);

/**
 * Reads an 16-bit integer value in big-endian
*/
int16_t br_read_int16BE(binary_reader *reader);

/**
 * Reads an 16-bit integer value in little-endian
*/
int16_t br_read_int16LE(binary_reader *reader);

/**
 * Reads an 32-bit integer value in big-endian
*/
int16_t br_read_int32BE(binary_reader *reader);

/**
 * Reads an 32-bit integer value in little-endian
*/
int16_t br_read_int32LE(binary_reader *reader);

/**
 * Frees the memory allocated by the binary reader
*/
void br_free(binary_reader *ptr);

binary_reader *br_new_from_file(FILE *f) {
  binary_reader *ptr = malloc(sizeof(binary_reader));

  fseek(f, 0, SEEK_END);
  int iend = ftell(f);
  fseek(f, 0, SEEK_SET);
  
  ptr->cursor = 0;
  ptr->size = iend;
  ptr->buffer = malloc(iend);

  fread(ptr->buffer, iend, 1, f);
  
  return ptr;
}

int8_t br_read_int8(binary_reader *reader) {
  int8_t vl = reader->buffer[reader->cursor];
  ++reader->cursor;
  return vl;
}

int16_t br_read_int16BE(binary_reader *reader) {
  int vl = reader->buffer[reader->cursor] << 8 | reader->buffer[reader->cursor + 1];
  reader->cursor += 2;
  return vl;
}

int16_t br_read_int16LE(binary_reader *reader) {
  int vl = reader->buffer[reader->cursor + 1] << 8 | reader->buffer[reader->cursor];
  reader->cursor += 2;
  return vl;
}

int16_t br_read_int32BE(binary_reader *reader) {
  int vl = reader->buffer[reader->cursor] << 24 | reader->buffer[reader->cursor + 1] << 16 | reader->buffer[reader->cursor + 2] << 8 | reader->buffer[reader->cursor + 3];
  reader->cursor += 4;
  return vl;
}

int16_t br_read_int32LE(binary_reader *reader) {
  int vl = reader->buffer[reader->cursor + 3] << 24 | reader->buffer[reader->cursor + 2] << 16 | reader->buffer[reader->cursor + 1] << 8 | reader->buffer[reader->cursor];
  reader->cursor += 4;
  return vl;
}

void br_free(binary_reader *ptr) {
  free(ptr->buffer);
  free(ptr);
}

//}
#endif

#ifdef KM_INCLUDE_JSON_FORMAT_PARSER
//{

enum json_element_type {
  JSON_NULL,
  JSON_INTEGER,
  JSON_FLOAT,
  JSON_STRING,
  JSON_OBJECT,
  JSON_ARRAY
};

struct json_element {
  enum json_element_type type;
  union {
    char *as_string;
    int as_int;
    float as_float;
    struct json_map *map;
  };
};

typedef struct json_element json_element_t;

struct json_map_entry {
  char *key;
  json_element_t* element;
};

struct json_map {
  struct json_map_entry **entries;
  size_t capacity;
  size_t size;
};

typedef struct json_element json_file_t;
typedef enum json_element_type json_element_type_t;

struct json_parsing_context {
  char _errorstr[256];
  char *strptr;
  size_t strlen;
  size_t line;
  size_t column;
  size_t cursor;
};

void json_skip_whitespace(struct json_parsing_context *context) {
  while (context->cursor < context->strlen && (context->strptr[context->cursor] == ' ' || context->strptr[context->cursor] == '\t' || context->strptr[context->cursor] == '\r' || context->strptr[context->cursor] == '\n')) {
    ++context->cursor;
  }
}

result parse_json_object(struct json_parsing_context *context) {
  ++context->cursor;

  json_skip_whitespace(context);

  if (context->strptr[context->cursor] == '\0') {
    result res = resultErr(("JsonParsingError: json object was not ended correctly. missing '}' "));
    return res;
  }

  if (context->strptr[context->cursor] == '"') {
    ++context->cursor;
    int lenKey = 0;
    int keyCurStart = context->cursor;
    while (context->cursor < context->strlen && context->strptr[context->cursor] != '"') {
      ++context->cursor;
      ++lenKey;
    }
    ++context->cursor;

    char *k = calloc(lenKey + 1, sizeof(char));
    for (size_t hj = 0; hj < lenKey; ++hj) {
      k[hj] = context->strptr[keyCurStart + hj];
    }
    k[lenKey] = '\0';

    json_skip_whitespace(context);

    if (context->strptr[context->cursor] != ':') {
      free(k);
      result res = resultErr(("JsonParsingError: missing ':' after key"));
      return res;
    }

    ++context->cursor;
    json_skip_whitespace(context);

    if (context->strptr[context->cursor] == '"') {
      ++context->cursor;
      int lenVL = 0;
      int VLCurStart = context->cursor;
      while (context->cursor < context->strlen && context->strptr[context->cursor] != '"') {
        ++context->cursor;
        ++lenVL;
      }
      ++context->cursor;

      char *vl = calloc(lenVL + 1, sizeof(char));
      for (size_t hj = 0; hj < lenVL; ++hj) {
        vl[hj] = context->strptr[VLCurStart + hj];
      }
      vl[lenVL] = '\0';

      printf("%s: %s", k, vl);

      // free(vl);
    }

    free(k);
  }

  result res = resultErr("JsonParsingError: bruh");
  return res;
}

result parse_json_file(char *strptr) {
  struct json_parsing_context context = { .strptr = strptr, .strlen = strlen(strptr), .line = 1, .column = 1, .cursor = 0 };

  json_element_t *base = malloc(sizeof(json_element_t));

  int len = strlen(strptr);

  if (len == 0) {
    struct result res = { NULL, "JsonParsingError: file is empty", true };
    return res;
  }

  size_t cursor = 0;

  while (cursor < len) {
    if (strptr[cursor] == ' ' || strptr[cursor] == '\r') {
      ++cursor;
    }

    if (strptr[cursor] == '{') {
      return parse_json_object(&context);
    }

    ++cursor;
  }

  struct result res = { base, NULL, false };
  return res;
}

bool is_json_object(json_element_t *element) {
  return element == NULL ? false : element->type == JSON_OBJECT;
}

bool is_json_array(json_element_t *element) {
  return element == NULL ? false : element->type == JSON_ARRAY;
}

bool is_json_string(json_element_t *element) {
  return element == NULL ? false : element->type == JSON_STRING;
}

bool is_json_int(json_element_t *element) {
  return element == NULL ? false : element->type == JSON_INTEGER;
}

bool is_json_float(json_element_t *element) {
  return element == NULL ? false : element->type == JSON_FLOAT;
}

bool is_json_null(json_element_t *element) {
  return element == NULL ? false : element->type == JSON_NULL;
}

void json_file_free(json_file_t *jf) {
  if (jf->type == JSON_STRING) {
    free(jf->as_string);
  } else if (jf->type == JSON_OBJECT) {
    for (int i = 0; i < jf->map->size; ++i) {
      free(jf->map->entries[i]->key);
      json_file_free(jf->map->entries[i]->element);
    }
    
    free(jf->map);
  }
  
  free(jf);
}

//}
#endif

#undef KM_SAFE_ITER_INDEX_REACH

#endif // _KM_R_HELPER_H_

/*

  FILE *f = fopen("test0.json", "r");

  char *text = read_file_as_string(f);

  printf("%s\n", text);

  result rjf = parse_json_file(text);

  if (!rjf.has_error) {
    json_file_t *jf = rjf.rvalue;
    json_file_free(jf);
  } else {
    printfln("%s", rjf.error);
    free(rjf.error);
  }

  fclose(f);
  free(text);

*/