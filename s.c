#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "kmhelper.h"

enum json_element_type {
  JSON_NULL,
  JSON_INTEGER,
  JSON_FLOAT,
  JSON_STRING,
  JSON_OBJECT,
  JSON_ARRAY
};

struct json_element {
  enum json_element_type type;
  union {
    char *as_string;
    int as_int;
    float as_float;
    struct json_map *map;
  };
};

typedef struct json_element json_element_t;

struct json_map_entry {
  char *key;
  json_element_t* element;
};

struct json_map {
  struct json_map_entry **entries;
  size_t capacity;
  size_t size;
};

typedef struct json_element json_file_t;
typedef enum json_element_type json_element_type_t;

typedef struct result {
  void *rvalue;
  char *error;
  bool has_error;
} result;

#define resultOk(value) { value, NULL, false }
#define resultErr(errormsg) { NULL, errormsg, true }

struct json_parsing_context {
  char *strptr;
  size_t strlen;
  size_t line;
  size_t column;
  size_t cursor;
};

void json_skip_whitespace(struct json_parsing_context *context) {
    while (context->cursor < context->strlen && (context->strptr[context->cursor] == ' ' || context->strptr[context->cursor] == '\t' || context->strptr[context->cursor] == '\r' || context->strptr[context->cursor] == '\n')) {
        ++context->cursor;
    }
}

result parse_json_object(struct json_parsing_context *context) {
  ++context->cursor;

  json_skip_whitespace(context);

  if (context->strptr[context->cursor] == '\0') {
    result res = resultErr(("JsonParsingError: json object was not ended correctly. missing '}' "));
    return res;
  }

  if (context->strptr[context->cursor] == '"') {
    
  }

  result res = resultErr("JsonParsingError: bruh");
  return res;
}

result parse_json_file(char *strptr) {
  struct json_parsing_context context = { strptr, strlen(strptr), 1, 1, 0 };

  json_element_t *base = malloc(sizeof(json_element_t));

  int len = strlen(strptr);

  if (len == 0) {
    struct result res = { NULL, "JsonParsingError: file is empty", true };
    return res;
  }

  size_t cursor = 0;

  while (cursor < len) {
    if (strptr[cursor] == ' ' || strptr[cursor] == '\r') {
      ++cursor;
    }

    if (strptr[cursor] == '{') {
      return parse_json_object(&context);
    }

    ++cursor;
  }

  struct result res = { base, NULL, false };
  return res;
}

bool is_json_object(json_element_t *element) {
  return element == NULL ? false : element->type == JSON_OBJECT;
}

bool is_json_array(json_element_t *element) {
  return element == NULL ? false : element->type == JSON_ARRAY;
}

bool is_json_string(json_element_t *element) {
  return element == NULL ? false : element->type == JSON_STRING;
}

bool is_json_int(json_element_t *element) {
  return element == NULL ? false : element->type == JSON_INTEGER;
}

bool is_json_float(json_element_t *element) {
  return element == NULL ? false : element->type == JSON_FLOAT;
}

bool is_json_null(json_element_t *element) {
  return element == NULL ? false : element->type == JSON_NULL;
}

void json_file_free(json_file_t *jf) {
  if (jf->type == JSON_STRING) {
    free(jf->as_string);
  } else if (jf->type == JSON_OBJECT) {
    for (int i = 0; i < jf->map->size; ++i) {
      free(jf->map->entries[i]->key);
      json_file_free(jf->map->entries[i]->element);
    }
    
    free(jf->map);
  }
  
  free(jf);
}

#include <stdarg.h>

void formatstring(char *dst, const char *fmt, ...) {
  va_list vl;
  va_start(vl, fmt);

  vsnprintf(dst, sizeof(dst), fmt, vl);

  va_end(vl);
}

int main(void) {
  FILE *f = fopen("test0.json", "r");

  char *text = read_file_as_string(f);

  printf("%s\n", text);

  result rjf = parse_json_file(text);

  if (!rjf.has_error) {
    json_file_t *jf = rjf.rvalue;
    json_file_free(jf);
  } else {
    printfln("%s", rjf.error);
  }

  fclose(f);
  free(text);
  return 0;
}